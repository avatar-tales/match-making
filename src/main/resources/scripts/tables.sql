CREATE TABLE IF NOT EXISTS `players`
(
    `uuid` VARCHAR(36) PRIMARY KEY NOT NULL,
    `name` VARCHAR(16),
    `elo`  INTEGER
);

CREATE TABLE IF NOT EXISTS `games`
(
    `uuid`   VARCHAR(36) PRIMARY KEY NOT NULL,
    `arena`  TEXT,
    `server` TEXT,
    `date`   DATE
);

CREATE TABLE IF NOT EXISTS `playing`
(
    `game`   VARCHAR(36),
    `player` VARCHAR(36),
    `status` TINYINT,
    FOREIGN KEY (`game`) REFERENCES `games` (`uuid`),
    FOREIGN KEY (`player`) REFERENCES `players` (`uuid`)
);

CREATE TABLE IF NOT EXISTS `events`
(
    `game`   VARCHAR(36),
    `player` VARCHAR(36),
    `action` VARCHAR(36),
    `date`   DATE,
    FOREIGN KEY (`game`) REFERENCES `games` (`uuid`),
    FOREIGN KEY (`player`) REFERENCES `players` (`uuid`)
);
