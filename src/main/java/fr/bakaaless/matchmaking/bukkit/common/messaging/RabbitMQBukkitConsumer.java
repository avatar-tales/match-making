package fr.bakaaless.matchmaking.bukkit.common.messaging;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.match.Arena;
import fr.bakaaless.matchmaking.bukkit.game.match.GameManager;
import fr.bakaaless.matchmaking.common.messaging.Message;
import fr.bakaaless.matchmaking.common.messaging.messages.PongPacket;
import fr.bakaaless.matchmaking.common.messaging.rabbitmq.RabbitMQ;
import fr.bakaaless.matchmaking.common.messaging.rabbitmq.RabbitMQConsumer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

public class RabbitMQBukkitConsumer extends RabbitMQConsumer {

    public RabbitMQBukkitConsumer(RabbitMQ rabbitMQ) {
        super(rabbitMQ);
        this.sendPong();
    }

    @Override
    public void accept(Message message) {
        if (message.type() == Message.MessageType.PING) {
            this.sendPong();
        }
    }

    public void sendPong() {
        final int port = MatchBukkit.get().getServerPort();
        try {
            super.instance.sendMessage(
                    new Message(
                            "proxy",
                            this.instance.getProperties().name(),
                            Message.MessageType.PONG,
                            super.instance.getGson().toJson(
                                    new PongPacket(
                                            this.instance.getProperties().name(),
                                            port,
                                            MatchBukkit.get().isLobby() ? new ArrayList<>() :
                                                    GameManager.getInstance().getFreePlayableArenas().stream().map(Arena::getSettings).toList(),
                                            MatchBukkit.get().isLobby()
                                    )
                            )
                    )
            );
        } catch (IOException | TimeoutException e) {
            throw new RuntimeException(e);
        }
    }
}
