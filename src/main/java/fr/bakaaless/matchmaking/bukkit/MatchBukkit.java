package fr.bakaaless.matchmaking.bukkit;

import fr.bakaaless.matchmaking.bukkit.game.commands.CommandManager;
import fr.bakaaless.matchmaking.bukkit.common.hook.Hook;
import fr.bakaaless.matchmaking.bukkit.game.hook.ProjectKorra;
import fr.bakaaless.matchmaking.bukkit.game.listeners.*;
import fr.bakaaless.matchmaking.bukkit.game.match.Arena;
import fr.bakaaless.matchmaking.bukkit.game.match.GameManager;
import fr.bakaaless.matchmaking.bukkit.game.match.GamePlayer;
import fr.bakaaless.matchmaking.bukkit.game.match.GameTeam;
import fr.bakaaless.matchmaking.bukkit.game.messaging.RabbitMQGameConsumer;
import fr.bakaaless.matchmaking.bukkit.lobby.commands.MainCommand;
import fr.bakaaless.matchmaking.bukkit.lobby.config.Messages;
import fr.bakaaless.matchmaking.bukkit.lobby.listeners.Connection;
import fr.bakaaless.matchmaking.bukkit.lobby.messaging.RabbitMQLobbyConsumer;
import fr.bakaaless.matchmaking.common.configuration.Config;
import fr.bakaaless.matchmaking.common.messaging.Message;
import fr.bakaaless.matchmaking.common.messaging.messages.Packet;
import fr.bakaaless.matchmaking.common.messaging.messages.game.End;
import fr.bakaaless.matchmaking.common.messaging.messages.game.StoppingServer;
import fr.bakaaless.matchmaking.common.messaging.messages.lobby.PlayerQueuePacket;
import fr.bakaaless.matchmaking.common.messaging.rabbitmq.RabbitMQ;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MatchBukkit extends JavaPlugin {

    private Config         config;
    private RabbitMQ       rabbitMQ;
    private Thread         rabbitConsumer;
    private CommandManager commandManager;
    private boolean    lobby;
    private List<Hook> hooks;

    private Config lobbyConfig;

    public static MatchBukkit get() {
        return JavaPlugin.getPlugin(MatchBukkit.class);
    }

    @Override
    public void onEnable() {
        this.hooks = new ArrayList<>();
        this.config = Config.load(this.getDataFolder().toPath(), "config.yml", "/bukkit/config.yml");
        try {
            this.config.load();
            this.lobby = this.config.get("type", "lobby").equalsIgnoreCase("lobby");
            if (this.lobby) {
                this.loadListeners(new Connection());
                Messages.init(this.getDataFolder().getAbsolutePath(), "messages.yml");

                this.lobbyConfig = Config.load(this.getDataFolder().toPath(), "lobby-config.yml", "/bukkit/lobby/config.yml");
                this.rabbitMQ = new RabbitMQ(this.getDataFolder().toPath());
                this.rabbitConsumer = this.rabbitMQ.retrieveMessages(new RabbitMQLobbyConsumer(this.rabbitMQ));
                this.rabbitConsumer.setName("RabbitMQLobbyConsumer");
                this.rabbitConsumer.setDaemon(true);
                this.rabbitConsumer.start();
                MainCommand commandManager = new MainCommand();
                this.getCommand("statistics").setExecutor(commandManager);
                this.getCommand("statistics").setTabCompleter(commandManager);
                this.getCommand("mjoin").setExecutor((sender, command, label, args) -> {
                    if (sender instanceof Player) this.sendPacket(new PlayerQueuePacket(((Player) sender).getUniqueId()), Message.MessageType.ADD_QUEUE);
                    return true;
                });
                this.getCommand("mleave").setExecutor((sender, command, label, args) -> {
                    if (sender instanceof Player) this.sendPacket(new PlayerQueuePacket(((Player) sender).getUniqueId()), Message.MessageType.REMOVE_QUEUE);
                    return true;
                });
            } else {
                this.loadListeners(new Blocks(), new Damage(), new Death(), new ItemHandler(), new Join(), new FoodLevel(), new Movement(), new Quit(), new Spawn(), new Spectate());

                this.commandManager = new CommandManager();
                this.getCommand("matchmakingeditor").setExecutor(this.commandManager);
                this.getCommand("matchmakingeditor").setTabCompleter(this.commandManager);

                GameManager gameManager = GameManager.fromFile(new File(this.getDataFolder(), "game").toPath());
                gameManager.load();
                this.getServer().getScheduler().runTaskTimer(this, gameManager::tick, 0, 1);
                if (this.getServer().getPluginManager().isPluginEnabled("ProjectKorra"))
                    this.hooks.add(new ProjectKorra());
                this.rabbitMQ = new RabbitMQ(this.getDataFolder().toPath());
                this.getServer().getScheduler().scheduleSyncDelayedTask(this, () -> {
                    this.rabbitConsumer = this.rabbitMQ.retrieveMessages(new RabbitMQGameConsumer(this.rabbitMQ));

                    this.rabbitConsumer.setName("RabbitMQGameConsumer");
                    this.rabbitConsumer.setDaemon(true);
                    this.rabbitConsumer.start();
                }, 1L);
            }
        } catch (Exception e) {
            this.getLogger().log(Level.SEVERE, "An error was caught while loading the plugin", e);
        }
    }

    @Override
    public void onDisable() {
        try {
            GameManager.getInstance().getPlayableArenas().stream().filter(arena -> !arena.isFree()).forEach(arena -> arena.stop(Arena.StopReason.INTERRUPT));
            this.rabbitConsumer.interrupt();
            this.rabbitMQ.sendMessage(new Message("proxy", this.rabbitMQ.getProperties().name(), Message.MessageType.STOPPING_SERVER, this.rabbitMQ.getGson().toJson(new StoppingServer())));
            GameManager.saveFile(GameManager.getInstance(), new File(this.getDataFolder(), "game").toPath());
        } catch (IOException | TimeoutException e) {
            this.getLogger().log(Level.SEVERE, "An error was caught while unloading the plugin", e);
        }
    }

    private void loadListeners(Listener... listeners) {
        for (Listener listener : listeners)
            this.getServer().getPluginManager().registerEvents(listener, this);
    }

    @Override
    public @NotNull Logger getLogger() {
        return super.getLogger();
    }

    public int getServerPort() {
        return this.getServer().getPort();
    }

    public CommandManager getCommandManager() {
        return this.commandManager;
    }

    public boolean isLobby() {
        return this.lobby;
    }

    public Config getLobbyConfig() {
        return this.lobbyConfig;
    }

    public RabbitMQ getRabbitMQ() {
        return this.rabbitMQ;
    }

    public boolean hasBend(UUID player) {
        return this.hooks.stream().filter(hook -> hook instanceof ProjectKorra).findFirst().filter(hook -> ((ProjectKorra) hook).hasBending(player)).isPresent();
    }

    public void sendVictory(UUID gameId, GameTeam winner, List<GameTeam> losers, List<GamePlayer> spectators, Arena.StopReason reason, boolean ranked) {
        UUID[] winnersUUID = winner.getAllPlayers().stream().map(GamePlayer::getUniqueId).toArray(UUID[]::new);
        UUID[] losersUUID = losers.stream().flatMap(team -> team.getAllPlayers().stream()).map(GamePlayer::getUniqueId).toArray(UUID[]::new);
        UUID[] spectatorsUUID = spectators.stream().map(GamePlayer::getUniqueId).toArray(UUID[]::new);

        End.EndReason reasonAdapted = switch (reason) {
            case WINNER, TIME_OUT -> End.EndReason.LEGIT;
            case INTERRUPT, ERROR -> End.EndReason.INTERRUPT;
        };
        this.sendPacket(new End(gameId, winnersUUID, losersUUID, spectatorsUUID, reasonAdapted, ranked), Message.MessageType.END);
    }

    public void sendPacket(Packet packet, Message.MessageType type) {
        try {
            this.rabbitMQ.sendMessage(new Message("proxy", this.rabbitMQ.getProperties().name(), type, this.rabbitMQ.getGson().toJson(packet)));
        } catch (IOException | TimeoutException e) {
            throw new RuntimeException(e);
        }
    }
}
