package fr.bakaaless.matchmaking.bukkit.game.commands.arena;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.commands.SubCmdInfo;
import fr.bakaaless.matchmaking.bukkit.game.match.Arena;
import fr.bakaaless.matchmaking.bukkit.game.match.GameMap;
import fr.bakaaless.matchmaking.common.utils.FileUtils;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.command.CommandSender;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

@SubCmdInfo(
        subcommand = "addmap",
        usage = "/mme arena addmap <world> <new_map>",
        description = "Add a world as a map for an arena",
        permission = "matchmaking.arena.addmap",
        arguments = 2,
        subArguments = { "<world>", "<new_map>" },
        executor = SubCmdInfo.Executor.PLAYER
)
public class AddMap extends Command {

    @Override
    public void run(CommandSender sender, String... args) {
        if (args.length <= 1) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cTwo arguments were expected : folder name and map name"));
            return;
        }
        if (!MatchBukkit.get().getCommandManager().getEditors().containsKey(sender)) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cYou are not editing any arena"));
            return;
        }
        Arena arena = MatchBukkit.get().getCommandManager().getEditors().get(sender);
        if (arena.getMaps().stream().anyMatch(map -> map.getId().equalsIgnoreCase(args[1]))) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cA map with this name is already used by this arena"));
            return;
        }
        File localFile = new File(MatchBukkit.get().getServer().getWorldContainer(), args[0]);
        if (!localFile.exists()) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cNo folder named like that was found in / folder"));
            return;
        }
        try {
            arena.getMaps().add(new GameMap(args[1]));
            File mapFolder = new File(MatchBukkit.get().getDataFolder(), "maps/" + arena.getSettings().getId());
            FileUtils.copyDirectory(localFile, new File(mapFolder, args[1]));
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&7Map added"));
        } catch (IOException e) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cAn error happened while copying folder"));
            MatchBukkit.get().getLogger().log(Level.SEVERE, "Can't copy map " + localFile.getName(), e);
        }
    }

}
