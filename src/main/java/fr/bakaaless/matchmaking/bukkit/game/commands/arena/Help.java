package fr.bakaaless.matchmaking.bukkit.game.commands.arena;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.commands.SubCmdInfo;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Comparator;

@SubCmdInfo(
        subcommand = "help",
        usage = "/mme help",
        description = "Get help about the plugin",
        permission = "matchmaking.arena.help"
)
public class Help extends Command {

    @Override
    public void run(CommandSender sender, String... args) {
        MatchBukkit.get().getCommandManager().getSubCommands().keySet().stream()
                .sorted(Comparator.comparing(SubCmdInfo::subcommand))
                .filter(subCmdInfo -> sender.hasPermission(subCmdInfo.permission()))
                .filter(subCmdInfo -> subCmdInfo.executor() == SubCmdInfo.Executor.PLAYER && sender instanceof Player || subCmdInfo.executor() == SubCmdInfo.Executor.CONSOLE && sender instanceof ConsoleCommandSender || subCmdInfo.executor() == SubCmdInfo.Executor.ALL)
                .forEach(subCommand ->
                    sender.sendMessage("§6" + subCommand.usage() + "§7: " + subCommand.description())
                );
    }

}
