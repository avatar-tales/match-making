package fr.bakaaless.matchmaking.bukkit.game.commands.arena;

import fr.bakaaless.matchmaking.bukkit.game.commands.SubCmdInfo;
import fr.bakaaless.matchmaking.bukkit.game.match.Arena;
import fr.bakaaless.matchmaking.bukkit.game.match.GameManager;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.command.CommandSender;

@SubCmdInfo(
        subcommand = "create",
        usage = "/mme create [<name>]",
        description = "Create an arena",
        permission = "matchmaking.arena.create",
        subArguments = { "[<name>]" },
        executor = SubCmdInfo.Executor.PLAYER
)
public class Create extends Command {

    @Override
    public void run(CommandSender sender, String... args) {
        Arena arena;
        if (args.length == 0)
            arena = new Arena();
        else {
            if (GameManager.getInstance().getArenas().stream().anyMatch(arenas -> arenas.getSettings().getName().equalsIgnoreCase(args[0]))) {
                super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cAn area with this name already exists"));
                return;
            }
            arena = new Arena(args[0]);
        }
        super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&7Creating area &b" + args[0] + " &7we are now in editing mode"));
        GameManager.getInstance().registerArena(arena);
    }

}
