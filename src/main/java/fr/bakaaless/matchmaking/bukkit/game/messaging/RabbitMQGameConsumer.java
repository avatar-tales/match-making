package fr.bakaaless.matchmaking.bukkit.game.messaging;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.common.messaging.RabbitMQBukkitConsumer;
import fr.bakaaless.matchmaking.bukkit.game.match.Arena;
import fr.bakaaless.matchmaking.bukkit.game.match.GameManager;
import fr.bakaaless.matchmaking.bukkit.game.match.GamePlayer;
import fr.bakaaless.matchmaking.common.messaging.Message;
import fr.bakaaless.matchmaking.common.messaging.messages.game.AddSpectators;
import fr.bakaaless.matchmaking.common.messaging.messages.game.RemoveSpectators;
import fr.bakaaless.matchmaking.common.messaging.messages.game.SendGroup;
import fr.bakaaless.matchmaking.common.messaging.messages.game.StartGame;
import fr.bakaaless.matchmaking.common.messaging.messages.other.HasBending;
import fr.bakaaless.matchmaking.common.messaging.messages.other.RequestBending;
import fr.bakaaless.matchmaking.common.messaging.rabbitmq.RabbitMQ;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeoutException;

public class RabbitMQGameConsumer extends RabbitMQBukkitConsumer {

    public RabbitMQGameConsumer(final RabbitMQ rabbitMQ) {
        super(rabbitMQ);
        this.sendPong();
    }

    @Override
    public void accept(final Message message) {
        super.accept(message);
        switch (message.type()) {
            case ADD_SPECTATOR -> {
                AddSpectators addSpectators = super.instance.getGson().fromJson(message.content(), AddSpectators.class);
                GamePlayer gamePlayer = GameManager.getInstance().getPlayers().get(addSpectators.getPlayer());
                if (gamePlayer == null || gamePlayer.getGame() == null || gamePlayer.getGame().isRanked() || gamePlayer.getPlayer().getPlayer() == null) {
                    try {
                        super.instance.sendMessage(
                                new Message(
                                        "proxy",
                                        this.instance.getProperties().name(),
                                        Message.MessageType.REMOVE_SPECTATOR,
                                        super.instance.getGson().toJson(new RemoveSpectators(addSpectators.getSpectators()))
                                )
                        );
                    } catch (IOException | TimeoutException e) {
                        throw new RuntimeException(e);
                    }
                    return;
                }
                MatchBukkit.get().getServer().getScheduler().runTask(MatchBukkit.get(), () -> {
                    Arrays.stream(addSpectators.getSpectators())
                            .map(GameManager.getInstance().getPlayers()::get)
                            .filter(Objects::nonNull)
                            .forEach(gamePlayers -> {
                                Player player = gamePlayers.getPlayer();
                                if (player == null) {
                                    System.out.println("Player not found");
                                    try {
                                        super.instance.sendMessage(
                                                new Message(
                                                        "proxy",
                                                        this.instance.getProperties().name(),
                                                        Message.MessageType.REMOVE_SPECTATOR,
                                                        super.instance.getGson().toJson(new RemoveSpectators(new UUID[]{gamePlayers.getUniqueId()}))
                                                )
                                        );
                                    } catch (IOException | TimeoutException e) {
                                        throw new RuntimeException(e);
                                    }
                                    return;
                                }
                                System.out.println(player.getName());
                                player.setGameMode(GameMode.SPECTATOR);
                                player.teleport(gamePlayer.getPlayer().getPlayer());
                                gamePlayer.getGame().getSpectators().add(gamePlayers);
                                for (Player players : gamePlayer.getGame().getPlayers().stream().map(GamePlayer::getPlayer).filter(Objects::nonNull).toArray(Player[]::new)) {
                                    player.showPlayer(MatchBukkit.get(), players);
                                }
                            });
                });
            }
            case SEND_GROUP -> {
                if (MatchBukkit.get().isLobby())
                    return;
                final SendGroup sendGroup = super.instance.getGson().fromJson(message.content(), SendGroup.class);
                Optional<Arena> arena = GameManager.getInstance().getFreePlayableArenas().stream().filter(arenas -> arenas.getSettings().matchType(sendGroup.getArenaSettings())).findAny();
                if (arena.isEmpty())
                    return;
                arena.get().setRanked(sendGroup.isRanked());
                if (!sendGroup.isRanked() && sendGroup.getRounds() > 0)
                    arena.get().setMaxPoints(sendGroup.getRounds());
                for (UUID playerUUID : sendGroup.getPlayers()) {
                    final GamePlayer gamePlayer = GameManager.getInstance().getPlayers().get(playerUUID);
                    if (gamePlayer == null)
                        continue;
                    gamePlayer.setRank(sendGroup.getRanks().get(playerUUID));
                    arena.get().addPlayer(gamePlayer);
                }
                MatchBukkit.get().getServer().getScheduler().runTaskLater(MatchBukkit.get(), () ->
                                arena.get().startGame()
                        , 20L);
                List<UUID[]> teams = arena.get().getTeams().stream().map(team -> team.getMates().stream().map(GamePlayer::getUniqueId).toArray(UUID[]::new)).toList();
                this.sendPong();
                try {
                    super.instance.sendMessage(
                            new Message(
                                    "proxy",
                                    this.instance.getProperties().name(),
                                    Message.MessageType.START_GAME,
                                    super.instance.getGson().toJson(new StartGame(arena.get().getSettings().getName(), arena.get().getGameId(), teams, sendGroup.isRanked()))
                            )
                    );
                } catch (IOException | TimeoutException e) {
                    throw new RuntimeException(e);
                }
            }
            case REQUEST_BENDING -> {
                try {
                    RequestBending requestBending = super.instance.getGson().fromJson(message.content(), RequestBending.class);
                    if (MatchBukkit.get().hasBend(requestBending.getPlayer())) {
                        super.instance.sendMessage(
                                new Message(
                                        "proxy",
                                        this.instance.getProperties().name(),
                                        Message.MessageType.HAS_BENDING,
                                        super.instance.getGson().toJson(new HasBending(requestBending.getPlayer()))
                                )
                        );
                    }
                } catch (IOException | TimeoutException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
