package fr.bakaaless.matchmaking.bukkit.game.commands.arena;

import fr.bakaaless.matchmaking.bukkit.game.commands.SubCmdInfo;
import net.kyori.adventure.text.Component;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class Command {

    public abstract void run(CommandSender sender, String... args);

    protected void output(CommandSender sender, Component component) {
        if (sender instanceof Player)
            sender.sendActionBar(component);
        else
            sender.sendMessage(component);
    }

    public String getName() {
        SubCmdInfo info = this.getClass().getAnnotation(SubCmdInfo.class);
        return info == null ? "" : info.subcommand();
    }

    public String getUsage() {
        SubCmdInfo info = this.getClass().getAnnotation(SubCmdInfo.class);
        return info == null ? "" : info.usage();
    }

    public String getDescription() {
        SubCmdInfo info = this.getClass().getAnnotation(SubCmdInfo.class);
        return info == null ? "" : info.description();
    }

    public String getPermission() {
        SubCmdInfo info = this.getClass().getAnnotation(SubCmdInfo.class);
        return info == null ? "" : info.permission();
    }

    public int getArgs() {
        SubCmdInfo info = this.getClass().getAnnotation(SubCmdInfo.class);
        return info == null ? 0 : info.arguments();
    }

    public SubCmdInfo.Executor getExecutor() {
        SubCmdInfo info = this.getClass().getAnnotation(SubCmdInfo.class);
        return info == null ? SubCmdInfo.Executor.ALL : info.executor();
    }

    public String[] getSubArgs() {
        SubCmdInfo info = this.getClass().getAnnotation(SubCmdInfo.class);
        return info == null ? new String[0] : info.subArguments();
    }

}
