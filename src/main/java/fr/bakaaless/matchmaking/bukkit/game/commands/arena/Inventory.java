package fr.bakaaless.matchmaking.bukkit.game.commands.arena;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.commands.SubCmdInfo;
import fr.bakaaless.matchmaking.bukkit.game.serializer.ItemUtils;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@SubCmdInfo(
        subcommand = "inventory",
        usage = "/mme inventory",
        description = "Set an inventory for an arena",
        permission = "matchmaking.arena.inventory",
        executor = SubCmdInfo.Executor.PLAYER
)
public class Inventory extends Command {

    @Override
    public void run(CommandSender sender, String... args) {
        if (!MatchBukkit.get().getCommandManager().getEditors().containsKey(sender)) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cYou are not editing any arena"));
            return;
        }
        if (sender instanceof Player player) {
            MatchBukkit.get().getCommandManager().getEditors().get(sender).getSettings().setInventory(ItemUtils.playerInventoryToBase64(player.getInventory()));
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&7Arena edited"));
            return;
        }
        super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cCommand for players only"));
    }

}
