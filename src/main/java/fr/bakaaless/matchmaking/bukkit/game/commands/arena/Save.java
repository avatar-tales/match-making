package fr.bakaaless.matchmaking.bukkit.game.commands.arena;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.commands.SubCmdInfo;
import fr.bakaaless.matchmaking.bukkit.game.match.Arena;
import fr.bakaaless.matchmaking.bukkit.game.match.GameManager;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.command.CommandSender;

import java.io.File;
import java.io.IOException;

@SubCmdInfo(
        subcommand = "save",
        usage = "/mme save",
        description = "Save arena and exit editing mode",
        permission = "matchmaking.arena.save",
        executor = SubCmdInfo.Executor.PLAYER
)
public class Save extends Command {

    @Override
    public void run(CommandSender sender, String... args) {
        if (!MatchBukkit.get().getCommandManager().getEditors().containsKey(sender)) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cYou are not editing any arena"));
            return;
        }
        Arena arena = MatchBukkit.get().getCommandManager().getEditors().remove(sender);
        try {
            GameManager.saveFile(GameManager.getInstance(), new File(MatchBukkit.get().getDataFolder(), "game").toPath());
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&7Arena saved"));
            arena.load();
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&7Arena reloaded"));
        } catch (IOException e) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cAn error occured while saving arena : " + e.getMessage()));
            e.printStackTrace();
        }
    }

}
