package fr.bakaaless.matchmaking.bukkit.game.listeners;

import com.destroystokyo.paper.event.player.PlayerStopSpectatingEntityEvent;
import fr.bakaaless.matchmaking.bukkit.game.match.GameManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class Spectate implements Listener {

    @EventHandler
    public void onPlayerStopSpectatingEntity(PlayerStopSpectatingEntityEvent event) {
        if (GameManager.getInstance().isPlayer(event.getPlayer().getUniqueId()))
            event.setCancelled(true);
    }
}
