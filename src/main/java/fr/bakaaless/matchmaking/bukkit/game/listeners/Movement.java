package fr.bakaaless.matchmaking.bukkit.game.listeners;

import fr.bakaaless.matchmaking.bukkit.game.match.Arena;
import fr.bakaaless.matchmaking.bukkit.game.match.GameManager;
import fr.bakaaless.matchmaking.bukkit.game.match.GamePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class Movement implements Listener {

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        if (GameManager.getInstance().isPlayer(event.getPlayer().getUniqueId())) {
            GamePlayer gamePlayer = GameManager.getInstance().getPlayers().get(event.getPlayer().getUniqueId());
            if (!gamePlayer.isInGame() || gamePlayer.getGame().getGameState() != Arena.State.STARTING)
                return;
            if (event.getFrom().getX() == event.getTo().getX() && event.getFrom().getY() == event.getTo().getY() && event.getFrom().getZ() == event.getTo().getZ()) {
                return;
            }
            event.setCancelled(true);
        }
    }
}
