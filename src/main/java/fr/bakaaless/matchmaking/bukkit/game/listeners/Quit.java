package fr.bakaaless.matchmaking.bukkit.game.listeners;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.match.GameManager;
import fr.bakaaless.matchmaking.bukkit.game.match.GamePlayer;
import net.kyori.adventure.text.Component;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class Quit implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        event.quitMessage(Component.empty());
        MatchBukkit.get().getCommandManager().getEditors().remove(event.getPlayer());
        if (GameManager.getInstance().isPlayer(event.getPlayer().getUniqueId())) {
            GamePlayer gamePlayer = GameManager.getInstance().getPlayers().get(event.getPlayer().getUniqueId());
            if (!gamePlayer.isInGame())
                return;
            gamePlayer.getGame().removePlayer(gamePlayer);
        }
    }

}
