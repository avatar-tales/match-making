package fr.bakaaless.matchmaking.bukkit.game.listeners;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.match.Arena;
import fr.bakaaless.matchmaking.bukkit.game.match.GameManager;
import fr.bakaaless.matchmaking.bukkit.game.match.GamePlayer;
import fr.bakaaless.matchmaking.common.messaging.Message;
import fr.bakaaless.matchmaking.common.messaging.messages.game.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class Death implements Listener {

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        event.setDroppedExp(0);
        event.getDrops().clear();
        if (!GameManager.getInstance().isPlayer(event.getPlayer().getUniqueId()))
            return;
        event.setCancelled(true);
        MatchBukkit.get().getServer().getScheduler().runTaskLater(MatchBukkit.get(), () -> {
            GamePlayer gamePlayer = GameManager.getInstance().getPlayers().get(event.getPlayer().getUniqueId());
            if (gamePlayer.isInGame() && gamePlayer.getGame().getGameState() == Arena.State.RUNNING) {
                if (event.deathMessage() != null)
                    gamePlayer.getGame().getPlayers().forEach(player -> player.getPlayer().sendMessage(event.deathMessage()));
                gamePlayer.getGame().death(gamePlayer);
                if (gamePlayer.getGame().isRanked()) {
                    MatchBukkit.get().sendPacket(new Event(gamePlayer.getGame().getGameId(), gamePlayer.getUniqueId(), Event.EventAction.DEATH), Message.MessageType.EVENT);
                    if (event.getPlayer().getKiller() != null)
                        MatchBukkit.get().sendPacket(new Event(gamePlayer.getGame().getGameId(), event.getPlayer().getKiller().getUniqueId(), Event.EventAction.KILL), Message.MessageType.EVENT);
                }
            }
        }, 1L);
    }
}
