package fr.bakaaless.matchmaking.bukkit.game.listeners;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.match.GameManager;
import fr.bakaaless.matchmaking.bukkit.game.match.GamePlayer;
import net.kyori.adventure.text.Component;
import org.bukkit.GameMode;
import org.bukkit.attribute.Attribute;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;

public class Join implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        GameManager.getInstance().getPlayers().put(event.getPlayer().getUniqueId(), new GamePlayer(event.getPlayer().getUniqueId()));
        event.joinMessage(Component.empty());
        event.getPlayer().getInventory().clear();
        event.getPlayer().getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(20);
        event.getPlayer().setHealth(event.getPlayer().getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
        event.getPlayer().setFoodLevel(20);
        event.getPlayer().setExp(0f);
        event.getPlayer().setLevel(0);
        event.getPlayer().setGameMode(GameMode.ADVENTURE);
        new ArrayList<>(event.getPlayer().getActivePotionEffects()).forEach(potion -> event.getPlayer().removePotionEffect(potion.getType()));
        MatchBukkit.get().getServer().getOnlinePlayers().forEach(players -> {
            GamePlayer gamePlayer = GameManager.getInstance().getPlayers().get(players.getUniqueId());
            if (!players.hasPermission("matchmaking.view") || gamePlayer != null && gamePlayer.isInGame())
                players.hidePlayer(MatchBukkit.get(), event.getPlayer());
            if (!event.getPlayer().hasPermission("matchmaking.view"))
                event.getPlayer().hidePlayer(MatchBukkit.get(), players);
        });
    }

}
