package fr.bakaaless.matchmaking.bukkit.game.commands.arena;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.commands.SubCmdInfo;
import fr.bakaaless.matchmaking.bukkit.game.match.Arena;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.command.CommandSender;

import java.io.File;

@SubCmdInfo(
        subcommand = "removemap",
        usage = "/mme removamap <map>",
        description = "Remove a map for an arena",
        permission = "matchmaking.arena.removemap",
        arguments = 1,
        subArguments = { "<map>" },
        executor = SubCmdInfo.Executor.PLAYER
)
public class RemoveMap extends Command {

    @Override
    public void run(CommandSender sender, String... args) {
        if (args.length == 0) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cOne argument was expected : map name"));
            return;
        }
        if (!MatchBukkit.get().getCommandManager().getEditors().containsKey(sender)) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cYou are not editing any arena"));
            return;
        }
        Arena arena = MatchBukkit.get().getCommandManager().getEditors().get(sender);
        if (arena.getMaps().stream().anyMatch(map -> map.getId().equalsIgnoreCase(args[0]))) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cA map with this name is already used by this arena"));
            return;
        }
        File mapFolder = new File(MatchBukkit.get().getDataFolder(), "maps/" + arena.getSettings().getId());
        arena.getMaps().removeIf(map -> {
            if (map.getId().equals(args[0])) {
                new File(mapFolder, map.getId()).delete();
                super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&7Map removed"));
                return true;
            }
            return false;
        });
    }

}
