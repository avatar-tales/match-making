package fr.bakaaless.matchmaking.bukkit.game.commands.arena;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.commands.SubCmdInfo;
import fr.bakaaless.matchmaking.bukkit.game.match.Arena;
import fr.bakaaless.matchmaking.bukkit.game.match.GameMap;
import fr.bakaaless.matchmaking.common.utils.FileUtils;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.command.CommandSender;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;

@SubCmdInfo(
        subcommand = "duplicate",
        usage = "/mme duplicate <map> <new_map>",
        description = "Duplicate a map for an arena",
        permission = "matchmaking.arena.duplicate",
        arguments = 2,
        subArguments = { "<map>", "<new_map>" },
        executor = SubCmdInfo.Executor.PLAYER
)
public class Duplicate extends Command {

    @Override
    public void run(CommandSender sender, String... args) {
        if (args.length <= 1) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cTwo arguments were expected : original map name and map name"));
            return;
        }
        if (!MatchBukkit.get().getCommandManager().getEditors().containsKey(sender)) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cYou are not editing any arena"));
            return;
        }
        Arena arena = MatchBukkit.get().getCommandManager().getEditors().get(sender);
        Optional<GameMap> original = arena.getMaps().stream().filter(map -> map.getId().equalsIgnoreCase(args[0])).findFirst();
        if (original.isEmpty()) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cA map with this name does not exist in this arena"));
            return;
        }
        if (arena.getMaps().stream().anyMatch(map -> map.getId().equalsIgnoreCase(args[1]))) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cA map with this name is already used by this arena"));
            return;
        }
        File localFile = new File(new File(MatchBukkit.get().getDataFolder(), "maps/" + arena.getSettings().getId()), args[0]);
        try {
            GameMap map = new GameMap(args[1]);
            original.get().getSpawns().forEach(spawn -> map.getSpawns().add(spawn.clone()));
            map.setDisplayName(original.get().getDisplayName());
            map.setLoaded(false);
            map.setPlayable(false);
            arena.getMaps().add(map);
            File mapFolder = new File(MatchBukkit.get().getDataFolder(), "maps/" + arena.getSettings().getId());
            FileUtils.copyDirectory(localFile, new File(mapFolder, args[1]));
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&7Map added"));
        } catch (IOException e) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cAn error happened while copying folder"));
            MatchBukkit.get().getLogger().log(Level.SEVERE, "Can't copy map " + localFile.getName(), e);
        }
    }

}
