package fr.bakaaless.matchmaking.bukkit.game.match;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.common.games.Rank;
import org.bukkit.entity.Player;

import java.util.UUID;

public class GamePlayer {

    private final UUID     uniqueId;
    private final Player   player;
    private       Rank     rank;
    private       Arena    game;
    private       GameTeam team;

    public GamePlayer(final UUID uniqueId) {
        this.uniqueId = uniqueId;
        this.player = MatchBukkit.get().getServer().getPlayer(uniqueId);
    }

    public UUID getUniqueId() {
        return this.uniqueId;
    }

    public Rank getRank() {
        return this.rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public boolean isInGame() {
        return this.game != null;
    }

    public Arena getGame() {
        return this.game;
    }

    public void setGame(Arena game) {
        this.game = game;
    }

    public GameTeam getTeam() {
        return this.team;
    }

    public void setTeam(GameTeam team) {
        this.team = team;
    }

    public Player getPlayer() {
        return this.player;
    }

    @Override
    public String toString() {
        return "GamePlayer{" +
                "uniqueId=" + uniqueId +
                ", player=" + player +
                ", rank=" + rank +
                ", game=" + game +
                ", team=" + team +
                '}';
    }
}
