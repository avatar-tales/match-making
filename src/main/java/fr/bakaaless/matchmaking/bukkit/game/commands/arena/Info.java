package fr.bakaaless.matchmaking.bukkit.game.commands.arena;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.commands.SubCmdInfo;
import fr.bakaaless.matchmaking.bukkit.game.match.Arena;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.command.CommandSender;

@SubCmdInfo(
        subcommand = "info",
        usage = "/mme info",
        description = "Retrieve info from an arena",
        permission = "matchmaking.arena.info",
        executor = SubCmdInfo.Executor.PLAYER
)
public class Info extends Command {

    @Override
    public void run(CommandSender sender, String... args) {
        if (!MatchBukkit.get().getCommandManager().getEditors().containsKey(sender)) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cYou are not editing any arena"));
            return;
        }
        Arena arena = MatchBukkit.get().getCommandManager().getEditors().get(sender);
        sender.sendMessage(arena.toString());
    }

}
