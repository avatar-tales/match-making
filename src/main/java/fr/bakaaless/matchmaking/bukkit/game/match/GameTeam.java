package fr.bakaaless.matchmaking.bukkit.game.match;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

public class GameTeam {

    private final List<GamePlayer> mates;
    private final List<GamePlayer> oldMembers;
    private       Arena            arena;
    private       GameLocation     spawn;
    private       int              point;

    public GameTeam() {
        this.mates = new ArrayList<>();
        this.oldMembers = new ArrayList<>();
        this.point = 0;
    }

    public List<GamePlayer> getMates() {
        return this.mates;
    }

    public List<GamePlayer> getOldMembers() {
        return this.mates;
    }

    public List<GamePlayer> getAllPlayers() {
        List<GamePlayer> players = new ArrayList<>();
        players.addAll(this.mates);
        players.addAll(this.oldMembers);
        return players;
    }

    public void removeMember(GamePlayer player) {
        if (!this.mates.contains(player))
            return;
        this.oldMembers.add(player);
        this.mates.remove(player);
    }

    public GameLocation getRawSpawn() {
        return this.spawn;
    }

    public Location getSpawn() {
        return new Location(
                MatchBukkit.get().getServer().getWorld(this.arena.getMap().getId() + "-" + this.arena.getSettings().getId()),
                this.spawn.getX(), this.spawn.getY(), this.spawn.getZ(), this.spawn.getYaw(), this.spawn.getPitch());
    }

    public void setSpawn(GameLocation spawn) {
        this.spawn = spawn;
    }

    public void setArena(Arena arena) {
        this.arena = arena;
    }

    public int getPoints() {
        return this.point;
    }

    public void addPoint() {
        this.point++;
    }
}
