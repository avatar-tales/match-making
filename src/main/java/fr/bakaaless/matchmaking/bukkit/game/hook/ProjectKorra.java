package fr.bakaaless.matchmaking.bukkit.game.hook;

import com.projectkorra.projectkorra.GeneralMethods;
import fr.bakaaless.matchmaking.bukkit.common.hook.Hook;

import java.util.UUID;

public class ProjectKorra implements Hook {

    public boolean hasBending(final UUID player) {
        return GeneralMethods.hasBend(player);
    }

}
