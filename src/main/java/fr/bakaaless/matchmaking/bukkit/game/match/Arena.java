package fr.bakaaless.matchmaking.bukkit.game.match;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.serializer.ItemUtils;
import fr.bakaaless.matchmaking.common.games.ArenaSettings;
import fr.bakaaless.matchmaking.common.utils.FileUtils;
import fr.mrmicky.fastboard.FastBoard;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import net.kyori.adventure.title.Title;
import org.bukkit.*;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.logging.Level;

public class Arena {

    private final ArenaSettings settings;
    private final List<GameMap> maps;
    private transient boolean free;
    private transient boolean playable;

    private transient UUID gameId;
    private transient GameMap map;
    private transient List<GameTeam> teams;
    private transient List<GamePlayer> spectators;
    private transient Map<GamePlayer, FastBoard> boards;
    private transient int timer;
    private transient int countdown;
    private transient State gameState;
    private transient int roundTick;
    private transient boolean ranked;
    private transient int maxPoints;

    public Arena() {
        this.settings = ArenaSettings.defaultSettings();
        this.settings.setName(this.settings.toString().substring(0, 4));
        this.maps = new ArrayList<>();
        this.load();
    }

    public Arena(String name) {
        this();
        this.settings.setName(name);
    }

    public void enable() {
        this.free = false;
        this.playable = true;
        this.loadMaps();
    }

    /**
     * Load the arena to be ready for next games.
     */
    public void load() {
        this.free = false;
        this.playable = true;
        this.gameId = UUID.randomUUID();
        this.map = null;
        this.teams = new ArrayList<>();
        this.spectators = new ArrayList<>();
        this.boards = new HashMap<>();
        this.timer = 0;
        this.countdown = 0;
        this.roundTick = 0;
        this.gameState = State.STARTING;
        this.ranked = false;
        this.maxPoints = this.getSettings().getPointToWin();

        List<GameMap> gameMaps = this.getLoadedMaps();

        // take a random map and then check if none arena is using it
        for (GameMap map : gameMaps) {
            if (map.isPlayable() && map.getSpawns().size() >= this.settings.getTeamsNumber()) {
                this.map = map;
                map.setPlayable(false);
                break;
            }
        }

        if (this.map == null) {
            this.playable = false;
            this.gameState = State.RESTARTING;
            return;
        }

        for (int i = 0; i < this.settings.getTeamsNumber(); i++) {
            this.teams.add(new GameTeam());
            this.teams.get(i).setArena(this);
            this.teams.get(i).setSpawn(this.map.getSpawns().get(i));
        }

        this.free = true;
    }

    /**
     * Load maps from the arena folder.
     */
    private void loadMaps() {
        Iterator<World> it = MatchBukkit.get().getServer().getWorlds().iterator();
        while (it.hasNext()) {
            World world = it.next();
            if (world.getName().contains("-" + this.settings.getId()))
                MatchBukkit.get().getServer().unloadWorld(world, false);
        }

        File mapFolder = new File(MatchBukkit.get().getDataFolder(), "maps/" + this.settings.getId());
        if (!mapFolder.exists())
            mapFolder.mkdirs();
        else if (!mapFolder.isDirectory()) {
            mapFolder.delete();
            mapFolder.mkdirs();
        } else if (mapFolder.listFiles() != null) {
            File worldContainer = MatchBukkit.get().getServer().getWorldContainer();
            File localFile;
            for (File file : mapFolder.listFiles()) {
                localFile = new File(worldContainer, file.getName() + "-" + this.settings.getId());
                if (localFile.exists())
                    localFile.delete();
                if (file.isDirectory()) {
                    try {
                        FileUtils.copyDirectory(file, localFile);
                    } catch (IOException e) {
                        MatchBukkit.get().getLogger().log(Level.SEVERE, "Can't copy map " + file.getName(), e);
                    }
                }
                new File(localFile, "uid.dat").delete();
                new File(localFile, "session.lock").delete();
            }
            this.maps.forEach(map -> {
                map.setLoaded(false);
                map.setPlayable(false);
                try {
                    if (!(new File(worldContainer, map.getId() + "-" + this.settings.getId())).exists())
                        return;
                    if (MatchBukkit.get().getServer().getWorld(map.getId() + "-" + this.settings.getId()) == null) {
                        World world = WorldCreator.name(map.getId() + "-" + this.settings.getId()).createWorld();
                        if (world == null)
                            return;
                        world.setTime(6000);
                        world.setWeatherDuration(0);
                        world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
                        world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);
                    }
                    map.setLoaded(true);
                    map.setPlayable(true);
                } catch (Exception e) {
                    MatchBukkit.get().getLogger().log(Level.SEVERE, "Can't load map " + map.getId(), e);
                }
            });
        }
    }

    public void startGame() {
        this.free = false;
        this.getPlayers().forEach(player -> {
            this.getPlayers().forEach(others -> {
                if (player != others)
                    player.getPlayer().showPlayer(MatchBukkit.get(), others.getPlayer());
            });
            this.boards.put(player, new FastBoard(player.getPlayer()));
        });
        this.startRound();
    }

    public void startRound() {
        this.gameState = State.STARTING;
        this.roundTick = 0;
        this.getPlayers().forEach(player -> {
            Player realPlayer = player.getPlayer();
            realPlayer.setGameMode(GameMode.ADVENTURE);
            realPlayer.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(20D);
            realPlayer.setHealth(realPlayer.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
            new ArrayList<>(realPlayer.getActivePotionEffects()).forEach(potion -> realPlayer.removePotionEffect(potion.getType()));
            realPlayer.getInventory().clear();
            try {
                this.giveItems(realPlayer);
            } catch (IOException e) {
                MatchBukkit.get().getLogger().log(Level.SEVERE, "Can't give items to player " + realPlayer.getName(), e);
                this.stop(StopReason.ERROR);
            }
            realPlayer.teleport(player.getTeam().getSpawn());
        });
    }

    public void tick() {
        this.timer++;
        if (this.gameState == State.STARTING || this.gameState == State.RUNNING) {
            this.getPlayers().forEach(player -> {
                FastBoard board = this.boards.get(player);
                board.updateTitle("§c§lRANKED");
                List<String> lore = new ArrayList<>(Arrays.asList(
                        "§7" + this.map.getDisplayName() + " §8@ §7" + this.settings.getName(),
                        "",
                        "§f§lTemps écoulé : §a" + this.parseTimer(),
                        "",
                        "§f§lAdversaires :"));
                this.getPlayers().stream().filter(opponent -> !Objects.equals(opponent.getTeam(), player.getTeam())).forEach(opponent ->
                        lore.add("§b" + opponent.getPlayer().getName() + (this.roundTick / 20 >= 300 ? " §a" + getDirection(player.getPlayer(), opponent.getPlayer()) : ""))
                );
                lore.add("");
                lore.add("§f§lÉlo : §a" + player.getRank().elo());
                lore.add("§f§lRang : §a" + ChatColor.translateAlternateColorCodes('&', player.getRank().display()));
                board.updateLines(lore);
            });
        }
        if (this.gameState == State.STARTING) {
            this.countdown++;
            if (this.countdown % 20 == 0) {
                Title title = Title.title(
                        LegacyComponentSerializer.legacyAmpersand().deserialize("&c&l" + (this.settings.getCountdown() - this.countdown / 20 + 1)),
                        Component.text(""),
                        Title.Times.times(
                                Duration.ofMillis(50),
                                Duration.ofSeconds(1),
                                Duration.ofMillis(50)
                        )
                );
                this.getPlayers().forEach(player -> {
                    player.getPlayer().showTitle(title);
                });
            }
            if (this.countdown / 20 > this.settings.getCountdown()) {
                this.gameState = State.RUNNING;
                this.countdown = 0;
            }
        } else if (this.gameState == State.RUNNING) {
            List<GameTeam> potentialWinners = this.teams.parallelStream()
                    .filter(team -> team.getMates().size() > 0)
                    .filter(team -> !allMemberDead(team))
                    .toList();
            if (potentialWinners.size() == 1) {
                GameTeam team = potentialWinners.get(0);
                team.addPoint();
                if (team.getPoints() >= this.maxPoints) {
                    this.stop(StopReason.WINNER);
                    return;
                }
                this.startRound();
            }
            if (this.roundTick++ / 20 >= 300) {
                this.getPlayers().stream()
                        .filter(player -> player.getPlayer() != null)
                        .filter(player -> player.getPlayer().getGameMode() != GameMode.SPECTATOR)
                        .forEach(player -> {
                            player.getPlayer().getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(2D);
                            player.getPlayer().setHealth(Math.min(player.getPlayer().getHealth(), player.getPlayer().getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue()));
                        });
            }
        } else if (this.gameState == State.ENDING) {
            this.countdown++;
            if (this.countdown / 20 > this.settings.getTimeBeforeKick()) {
                this.getPlayers().forEach(player -> {
                    this.removePlayer(player);
                    player.getPlayer().kick(Component.text("Game ended"), PlayerKickEvent.Cause.PLUGIN);
                });
                this.load();
            }
        }
    }

    public void stop(StopReason reason) {
        this.getPlayers().forEach(gamePlayer -> {
            try {
                if (this.boards.containsKey(gamePlayer) && this.boards.get(gamePlayer) != null && !this.boards.get(gamePlayer).isDeleted()) {
                    try {
                        this.boards.get(gamePlayer).delete();
                    } catch (Exception ignored) {
                    }
                    this.boards.remove(gamePlayer);
                }
                Optional.ofNullable(gamePlayer.getPlayer()).ifPresent(player -> {
                    player.setGameMode(GameMode.ADVENTURE);
                    player.getInventory().clear();
                    this.getPlayers().forEach(players -> {
                        if (players != gamePlayer) {
                            Optional.ofNullable(players.getPlayer()).ifPresent(other -> player.hidePlayer(MatchBukkit.get(), other));
                        }
                    });
                });
            } catch (Exception e) {
                MatchBukkit.get().getLogger().log(Level.SEVERE, "Can't stop game", e);
            }
        });
        this.gameState = State.ENDING;
        GameTeam winners = this.teams.stream().max(Comparator.comparing(GameTeam::getPoints)).get();
        List<GameTeam> losers = this.teams.stream().filter(team -> team != winners).toList();
        MatchBukkit.get().sendVictory(this.gameId, winners, losers, this.spectators, reason, this.ranked);
    }

    public void death(final GamePlayer gamePlayer) {
        Player player = gamePlayer.getPlayer();
        player.setGameMode(GameMode.SPECTATOR);
        if (this.allMemberDead(gamePlayer.getTeam())) {
            List<GameTeam> potentialWinners = this.teams.parallelStream()
                    .filter(team -> team.getMates().size() > 0)
                    .filter(team -> !allMemberDead(team))
                    .toList();
            if (potentialWinners.size() == 1) {
                GameTeam team = potentialWinners.get(0);
                team.addPoint();
                if (team.getPoints() >= this.maxPoints) {
                    this.stop(StopReason.WINNER);
                    return;
                }
                this.startRound();
            } else {
                potentialWinners.stream().findAny().flatMap(team -> team.getMates().stream().findAny()).ifPresent(opponent -> {
                    Player opponentPlayer = opponent.getPlayer().getPlayer();
                    for (GamePlayer allPlayer : this.getPlayers()) {
                        if (allPlayer.getPlayer().getSpectatorTarget() == player)
                            allPlayer.getPlayer().setSpectatorTarget(opponentPlayer);
                    }
                });
            }
        } else {
            gamePlayer.getTeam().getMates().stream()
                    .filter(mate -> mate.getPlayer().getGameMode() != GameMode.SPECTATOR)
                    .findAny()
                    .ifPresent(mate -> {
                        Player matePlayer = mate.getPlayer().getPlayer();
                        for (GamePlayer allPlayer : this.getPlayers()) {
                            if (allPlayer.getPlayer().getSpectatorTarget() == player)
                                allPlayer.getPlayer().setSpectatorTarget(matePlayer);
                        }
                        gamePlayer.getPlayer().setSpectatorTarget(matePlayer);
                    });
        }

    }

    private boolean allMemberDead(GameTeam team) {
        return team.getMates().parallelStream().allMatch(player -> player.getPlayer().getGameMode() == GameMode.SPECTATOR);
    }

    /**
     * Give the player the items he needs to play the game.
     *
     * @param player the player to give the items to.
     * @throws IOException if the string value is not a valid inventory.
     */
    private void giveItems(final Player player) throws IOException {
        final ItemStack[] content = ItemUtils.itemStackArrayFromBase64(this.settings.getInventory()[0]);
        for (int index = 0; index < content.length; index++) {
            final ItemStack item = content[index];
            if (item == null)
                continue;
            content[index] = item;
        }
        player.getInventory().setContents(content);
        final ItemStack[] armor = ItemUtils.itemStackArrayFromBase64(this.settings.getInventory()[1]);
        for (int index = 0; index < armor.length; index++) {
            final ItemStack item = armor[index];
            if (item == null)
                continue;
            armor[index] = item;
        }
        player.getInventory().setArmorContents(armor);
        final ItemStack[] extraContent = ItemUtils.itemStackArrayFromBase64(this.settings.getInventory()[2]);
        for (int index = 0; index < extraContent.length; index++) {
            final ItemStack item = extraContent[index];
            if (item == null)
                continue;
            extraContent[index] = item;
        }
        player.getInventory().setExtraContents(extraContent);
    }

    public void addPlayer(GamePlayer player) {
        this.teams.stream().min(Comparator.comparingInt(game -> game.getMates().size())).ifPresent(team -> {
            team.getMates().add(player);
            player.setTeam(team);
            player.setGame(this);
        });
    }

    public void removePlayer(GamePlayer player) {
        player.getTeam().removeMember(player);
        this.boards.remove(player);
    }

    private String parseTimer() {
        StringBuilder builder = new StringBuilder();
        int minutes = (this.timer / 20) / 60;
        int seconds = (this.timer / 20) % 60;
        if (minutes < 10)
            builder.append("0");
        builder.append(minutes);
        builder.append(":");
        if (seconds < 10)
            builder.append("0");
        builder.append(seconds);
        return builder.toString();
    }

    public ArenaSettings getSettings() {
        return settings;
    }

    public List<GameMap> getMaps() {
        return this.maps;
    }

    public List<GameMap> getLoadedMaps() {
        return this.maps.stream().filter(GameMap::isLoaded).toList();
    }

    public List<GameMap> getPlayableMaps() {
        return this.maps.stream().filter(GameMap::isPlayable).toList();
    }

    public UUID getGameId() {
        return this.gameId;
    }

    public GameMap getMap() {
        return this.map;
    }

    public List<GameTeam> getTeams() {
        return this.teams;
    }

    public State getGameState() {
        return this.gameState;
    }

    public boolean isFree() {
        return this.free;
    }

    public boolean isPlayable() {
        return this.playable || this.getPlayableMaps().size() > 0;
    }

    public List<GamePlayer> getPlayers() {
        return this.teams.stream().flatMap(team -> team.getMates().stream()).toList();
    }

    public List<GamePlayer> getSpectators() {
        return this.spectators;
    }

    public void setRanked(boolean ranked) {
        this.ranked = ranked;
    }

    public boolean isRanked() {
        return this.ranked;
    }

    public int getMaxPoints() {
        return this.maxPoints;
    }

    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }

    /**
     * Code par SamaGames
     * <p>
     * https://github.com/SamaGames/SurvivalAPI/blob/master/src/main/java/net/samagames/survivalapi/game/SurvivalGameLoop.java#L330
     **/
    public static String getDirection(Player player, Player opponent) {
        Location ploc = player.getLocation();
        Location point = opponent.getLocation();

        if (ploc.getWorld().getEnvironment() != point.getWorld().getEnvironment() || opponent.getGameMode() == GameMode.SPECTATOR)
            return "•";

        ploc.setY(0);
        point.setY(0);

        Vector d = ploc.getDirection();
        Vector v = point.subtract(ploc).toVector().normalize();

        double a = Math.toDegrees(Math.atan2(d.getX(), d.getZ()));
        a -= Math.toDegrees(Math.atan2(v.getX(), v.getZ()));
        a = (int) (a + 22.5) % 360;

        if (a < 0)
            a += 360;

        return Character.toString("⬆⬈➡⬊⬇⬋⬅⬉".charAt((int) a / 45));
    }


    public enum State {
        STARTING,
        RUNNING,
        ENDING,
        RESTARTING
    }

    public enum StopReason {
        WINNER,
        TIME_OUT,
        INTERRUPT,
        ERROR
    }

    @Override
    public String toString() {
        return "Arena{" +
                "settings=" + settings +
                ", maps=" + maps +
                ", free=" + free +
                ", playable=" + playable +
                ", gameId=" + gameId +
                ", map=" + map +
                ", teams=" + teams +
                ", spectators=" + spectators +
                ", boards=" + boards +
                ", timer=" + timer +
                ", countdown=" + countdown +
                ", gameState=" + gameState +
                '}';
    }
}
