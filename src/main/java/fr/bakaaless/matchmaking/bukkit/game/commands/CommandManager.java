package fr.bakaaless.matchmaking.bukkit.game.commands;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.commands.arena.*;
import fr.bakaaless.matchmaking.bukkit.game.match.Arena;
import fr.bakaaless.matchmaking.bukkit.game.match.GameManager;
import fr.bakaaless.matchmaking.bukkit.game.match.GameMap;
import org.bukkit.World;
import org.bukkit.command.*;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Stream;

public class CommandManager implements CommandExecutor, TabCompleter {

    private final Map<CommandSender, Arena> editors;
    private final Map<SubCmdInfo, fr.bakaaless.matchmaking.bukkit.game.commands.arena.Command>  subCommands;

    public CommandManager() {
        this.editors = new HashMap<>();
        this.subCommands = new HashMap<>();
        this.registerCommands(
                new AddMap(),
                new AddSpawn(),
                new Create(),
                new Delete(),
                new Duplicate(),
                new Edit(),
                new Help(),
                new Inventory(),
                new Info(),
                new RemoveMap(),
                new RemoveSpawn(),
                new Save(),
                new Teleport()
        );
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (args.length == 0)
            args = new String[]{""};
        @NotNull String[] finalArgs = args;
        this.subCommands.keySet().stream()
                .filter(subCommand -> subCommand.subcommand().equalsIgnoreCase(finalArgs[0]))
                .min(Comparator.comparing(SubCmdInfo::subcommand))
                .ifPresentOrElse(subCommand -> {
                            try {
                                if (subCommand.arguments() > finalArgs.length) {
                                    sender.sendMessage("Trop peu d'arguments");
                                    return;
                                }
                                if (!sender.hasPermission(subCommand.permission())) {
                                    return;
                                }
                                if (subCommand.executor() == SubCmdInfo.Executor.ALL)
                                    this.subCommands.get(subCommand).run(sender, Arrays.stream(finalArgs).skip(1).toArray(String[]::new));
                                else if (subCommand.executor() == SubCmdInfo.Executor.PLAYER && sender instanceof Player player)
                                    this.subCommands.get(subCommand).run(player, Arrays.stream(finalArgs).skip(1).toArray(String[]::new));
                                else if (subCommand.executor() == SubCmdInfo.Executor.CONSOLE && sender instanceof ConsoleCommandSender console)
                                    this.subCommands.get(subCommand).run(console, Arrays.stream(finalArgs).skip(1).toArray(String[]::new));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }, () ->
                                new Help().run(sender, Arrays.stream(finalArgs).skip(1).toArray(String[]::new))
                );
        return false;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (args.length == 0)
            args = new String[]{""};
        @NotNull String[] finalArgs = args;
        if (args.length == 1)
            return this.subCommands.keySet().stream()
                    .filter(subCommand ->
                            subCommand.permission().isBlank() ||
                                    sender.hasPermission(subCommand.permission())
                    )
                    .filter(subCommand ->
                            subCommand.executor() == SubCmdInfo.Executor.ALL ||
                                    subCommand.executor() == SubCmdInfo.Executor.PLAYER && sender instanceof Player ||
                                    subCommand.executor() == SubCmdInfo.Executor.CONSOLE && sender instanceof ConsoleCommandSender
                    )
                    .map(SubCmdInfo::subcommand)
                    .filter(subCommand -> subCommand.toLowerCase().startsWith(finalArgs[0].toLowerCase()))
                    .toList();
        else {
            String[] subArgs = Arrays.stream(finalArgs).skip(1).toArray(String[]::new);
            return this.subCommands.keySet().stream()
                    .filter(subCommand ->
                            subCommand.permission().isBlank() ||
                                    sender.hasPermission(subCommand.permission())
                    )
                    .filter(subCommand ->
                            subCommand.executor() == SubCmdInfo.Executor.ALL ||
                                    subCommand.executor() == SubCmdInfo.Executor.PLAYER && sender instanceof Player ||
                                    subCommand.executor() == SubCmdInfo.Executor.CONSOLE && sender instanceof ConsoleCommandSender
                    )
                    .filter(subCommand -> subCommand.arguments() >= subArgs.length)
                    .filter(subCommand -> subCommand.subcommand().equals(finalArgs[0]))
                    .filter(subCommand -> {
                        for (int i = 0; i < subArgs.length && i < subCommand.subArguments().length; i++) {
                            String lookingArg = subCommand.subArguments()[i];
                            if (!lookingArg.startsWith("<") && !lookingArg.startsWith("[") && !lookingArg.equalsIgnoreCase(subArgs[i + 1]))
                                return false;
                        }
                        String lastArg = subCommand.subArguments()[subArgs.length - 1];
                        return lastArg.startsWith("<") || lastArg.startsWith("[") || lastArg.toLowerCase().startsWith(subArgs[subArgs.length - 1].toLowerCase());
                    })
                    .flatMap(subCommand -> {
                        String lastArg = subCommand.subArguments()[subArgs.length - 1];
                        if (lastArg.startsWith("<") || lastArg.startsWith("[")) {
                            if (lastArg.contains("arena")) {
                                return GameManager.getInstance().getArenas().stream().map(arena -> arena.getSettings().getName());
                            }
                            if (lastArg.contains("map") && this.editors.containsKey(sender)) {
                                return this.editors.get(sender).getMaps().stream().map(GameMap::getId);
                            }
                            if (lastArg.contains("world")) {
                                return MatchBukkit.get().getServer().getWorlds().stream().map(World::getName);
                            }
                        }
                        return Stream.of(lastArg);
                    })
                    .toList();
        }
    }

    public Map<CommandSender, Arena> getEditors() {
        return this.editors;
    }

    public Map<SubCmdInfo, fr.bakaaless.matchmaking.bukkit.game.commands.arena.Command> getSubCommands() {
        return this.subCommands;
    }

    public void registerCommands(fr.bakaaless.matchmaking.bukkit.game.commands.arena.Command... commands) {
        for (var command : commands) {
            this.subCommands.put(command.getClass().getAnnotation(SubCmdInfo.class), command);
        }

    }

}
