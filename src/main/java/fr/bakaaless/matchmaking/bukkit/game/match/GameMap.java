package fr.bakaaless.matchmaking.bukkit.game.match;

import java.util.ArrayList;
import java.util.List;

public class GameMap {

    private final     String             name;
    private           String             displayName;
    private final     List<GameLocation> spawns;
    private transient boolean            load;
    private transient boolean            playable;

    public GameMap(String name) {
        this.name = name;
        this.displayName = name;
        this.spawns = new ArrayList<>();
        this.load = false;
        this.playable = false;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getId() {
        return this.name;
    }

    public List<GameLocation> getSpawns() {
        return this.spawns;
    }

    public boolean isLoaded() {
        return this.load;
    }

    public void setLoaded(boolean load) {
        this.load = load;
    }

    public boolean isPlayable() {
        return this.playable;
    }

    public void setPlayable(boolean playable) {
        this.playable = playable;
    }

    @Override
    public String toString() {
        return "GameMap{" +
                "name='" + name + '\'' +
                ", displayName='" + displayName + '\'' +
                ", spawns=" + spawns +
                ", load=" + load +
                ", playable=" + playable +
                '}';
    }
}
