package fr.bakaaless.matchmaking.bukkit.game.commands.arena;

import fr.bakaaless.matchmaking.bukkit.game.commands.SubCmdInfo;
import fr.bakaaless.matchmaking.bukkit.game.match.GameManager;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.command.CommandSender;

@SubCmdInfo(
        subcommand = "delete",
        usage = "/mme delete <arena>",
        description = "Delete an arena",
        permission = "matchmaking.arena.delete",
        arguments = 1,
        subArguments = { "<arena>" },
        executor = SubCmdInfo.Executor.PLAYER
)
public class Delete extends Command {

    @Override
    public void run(CommandSender sender, String... args) {
        if (args.length == 0) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cAn argument was expected"));
            return;
        } else {
            if (GameManager.getInstance().getArenas().stream().anyMatch(arenas -> arenas.getSettings().getName().equalsIgnoreCase(args[0]))) {
                GameManager.getInstance().getArenas().removeIf(arena -> arena.getSettings().getName().equalsIgnoreCase(args[0]));
                super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&7Arena &b" + args[0] + " &7deleted"));
                return;
            }
        }
        super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cNo arena was found"));
    }

}
