package fr.bakaaless.matchmaking.bukkit.game.commands.arena;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.commands.SubCmdInfo;
import fr.bakaaless.matchmaking.bukkit.game.match.Arena;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@SubCmdInfo(
        subcommand = "teleport",
        usage = "/mme teleport <map>",
        description = "Teleport a map of an arena",
        permission = "matchmaking.arena.teleport",
        arguments = 1,
        subArguments = { "<map>" },
        executor = SubCmdInfo.Executor.PLAYER
)
public class Teleport extends Command {

    @Override
    public void run(CommandSender sender, String... args) {
        if (args.length == 0) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cAn argument was expected : map name"));
            return;
        }
        if (!MatchBukkit.get().getCommandManager().getEditors().containsKey(sender)) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cYou are not editing any arena"));
            return;
        }
        if (sender instanceof Player player) {
            Arena arena = MatchBukkit.get().getCommandManager().getEditors().get(player);
            if (arena.getMaps().stream().noneMatch(map -> map.getId().equalsIgnoreCase(args[0]))) {
                super.output(player, LegacyComponentSerializer.legacyAmpersand().deserialize("&cNo map with this name was found"));
                return;
            }
            arena.getMaps().stream().filter(map -> map.getId().equalsIgnoreCase(args[0])).findFirst().ifPresent(map -> {
                if (MatchBukkit.get().getServer().getWorld(map.getId() + "-" + arena.getSettings().getId()) == null) {
                    super.output(player, LegacyComponentSerializer.legacyAmpersand().deserialize("&cThis map is not load by the server. Please reload"));
                    return;
                }
                player.teleport(MatchBukkit.get().getServer().getWorld(map.getId() + "-" + arena.getSettings().getId()).getSpawnLocation());
                super.output(player, LegacyComponentSerializer.legacyAmpersand().deserialize("&7Teleported"));
            });
            return;
        }
        super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cCommand for players only"));
    }

}
