package fr.bakaaless.matchmaking.bukkit.game.match;

import org.bukkit.Location;

public class GameLocation {

    private final double x;
    private final double y;
    private final double z;
    private final float  yaw;
    private final float  pitch;

    public GameLocation(double x, double y, double z, float yaw, float pitch) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public static GameLocation convert(Location location) {
        return new GameLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public float getYaw() {
        return yaw;
    }

    public float getPitch() {
        return pitch;
    }

    public double distance(GameLocation gameLocation) {
        return Math.sqrt(Math.pow(this.x - gameLocation.x, 2) + Math.pow(this.y - gameLocation.y, 2) + Math.pow(this.z - gameLocation.z, 2));
    }

    public GameLocation clone() {
        return new GameLocation(this.x, this.y, this.z, this.yaw, this.pitch);
    }

}
