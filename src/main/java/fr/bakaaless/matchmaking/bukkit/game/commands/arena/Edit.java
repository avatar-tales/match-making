package fr.bakaaless.matchmaking.bukkit.game.commands.arena;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.commands.SubCmdInfo;
import fr.bakaaless.matchmaking.bukkit.game.match.GameManager;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.command.CommandSender;

@SubCmdInfo(
        subcommand = "edit",
        usage = "/mme edit <arena>",
        description = "Edit an arena",
        permission = "matchmaking.arena.edit",
        arguments = 1,
        subArguments = { "<arena>" },
        executor = SubCmdInfo.Executor.PLAYER
)
public class Edit extends Command {

    @Override
    public void run(CommandSender sender, String... args) {
        if (args.length == 0) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cAn argument was expected : arena name"));
            return;
        }
        if (MatchBukkit.get().getCommandManager().getEditors().containsKey(sender)) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cYou are already editing an arena"));
            return;
        }
        if (GameManager.getInstance().getArenas().stream().anyMatch(arenas -> arenas.getSettings().getName().equalsIgnoreCase(args[0]))) {
            if (MatchBukkit.get().getCommandManager().getEditors().values().stream().anyMatch(arena -> arena.getSettings().getName().equalsIgnoreCase(args[0]))) {
                super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cThis area is already editing by someone"));
                return;
            }
            GameManager.getInstance().getArenas().stream()
                    .filter(arena ->
                            arena.getSettings().getName().equalsIgnoreCase(args[0])
                    )
                    .findFirst()
                    .ifPresent(arena ->
                            MatchBukkit.get().getCommandManager().getEditors().put(sender, arena)
                    );
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&7You are editing arena &b" + args[0]));
            return;
        }
        super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cNo arena was found"));
    }

}
