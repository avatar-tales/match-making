package fr.bakaaless.matchmaking.bukkit.game.listeners;

import fr.bakaaless.matchmaking.bukkit.game.match.Arena;
import fr.bakaaless.matchmaking.bukkit.game.match.GameManager;
import fr.bakaaless.matchmaking.bukkit.game.match.GamePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class Damage implements Listener {

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent event) {
        if (!GameManager.getInstance().isPlayer(event.getEntity().getUniqueId()))
            return;
        GamePlayer gamePlayer = GameManager.getInstance().getPlayers().get(event.getEntity().getUniqueId());
        if (!gamePlayer.isInGame() || gamePlayer.getGame().getGameState() != Arena.State.RUNNING) {
            event.setCancelled(true);
        }
    }
}
