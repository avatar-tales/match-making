package fr.bakaaless.matchmaking.bukkit.game.commands;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface SubCmdInfo {

    String subcommand();

    String permission() default "";

    String usage();

    String description() default "";

    int arguments() default 0;

    Executor executor() default Executor.ALL;

    String[] subArguments() default {};


    enum Executor {
        PLAYER,
        CONSOLE,
        ALL
    }

}
