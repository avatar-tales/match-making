package fr.bakaaless.matchmaking.bukkit.game.commands.arena;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.game.commands.SubCmdInfo;
import fr.bakaaless.matchmaking.bukkit.game.match.Arena;
import fr.bakaaless.matchmaking.bukkit.game.match.GameLocation;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Comparator;
import java.util.Optional;

@SubCmdInfo(
        subcommand = "removespawn",
        usage = "/mme removespawn <map>",
        description = "Delete nearest spawn from an arena's map",
        permission = "matchmaking.arena.removespawn",
        arguments = 1,
        subArguments = { "<map>" },
        executor = SubCmdInfo.Executor.PLAYER
)
public class RemoveSpawn extends Command {

    @Override
    public void run(CommandSender sender, String... args) {
        if (args.length == 0) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cAn argument was expected : map name"));
            return;
        }
        if (!MatchBukkit.get().getCommandManager().getEditors().containsKey(sender)) {
            super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cYou are not editing any arena"));
            return;
        }
        if (sender instanceof Player player) {
            Arena arena = MatchBukkit.get().getCommandManager().getEditors().get(player);
            if (arena.getMaps().stream().noneMatch(map -> map.getId().equalsIgnoreCase(args[0]))) {
                super.output(player, LegacyComponentSerializer.legacyAmpersand().deserialize("&cNo map with this name was found"));
                return;
            }
            arena.getMaps().stream().filter(map -> map.getId().equalsIgnoreCase(args[0])).findFirst().ifPresent(map -> {
                GameLocation playerLocation = GameLocation.convert(player.getLocation());
                Optional<GameLocation> spawn = map.getSpawns().stream().min(Comparator.comparingDouble(location -> location.distance(playerLocation)));
                if (spawn.isEmpty()) {
                    super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cNo spawn was found"));
                    return;
                }
                map.getSpawns().remove(spawn.get());
                super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&7Removed nearest spawn"));
            });
            return;
        }
        super.output(sender, LegacyComponentSerializer.legacyAmpersand().deserialize("&cCommand for players only"));
    }

}
