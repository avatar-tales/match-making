package fr.bakaaless.matchmaking.bukkit.game.match;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.bakaaless.matchmaking.bukkit.MatchBukkit;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class GameManager {

    private static final Gson                      gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().serializeNulls().create();
    private static       GameManager               instance;
    private final        List<Arena>               arenas;
    private transient    HashMap<UUID, GamePlayer> players;


    public GameManager() {
        this.arenas = new ArrayList<>();
    }

    public static GameManager getInstance() {
        if (instance == null)
            instance = new GameManager();
        return instance;
    }

    public static GameManager fromFile(Path directory) throws IOException {
        File settings = getFileSettings(directory);
        if (!settings.exists())
            saveFile(getInstance(), directory);
        instance = gson.fromJson(Files.readString(settings.toPath(), StandardCharsets.UTF_8), GameManager.class);
        return instance;
    }

    public static void saveFile(GameManager gameManager, Path directory) throws IOException {
        File settings = getFileSettings(directory);
        if (!settings.exists()) {
            settings.getParentFile().mkdirs();
            settings.createNewFile();
        } else {
            settings.delete();
            settings.createNewFile();
        }
        Files.writeString(getFileSettings(directory).toPath(), gson.toJson(gameManager), StandardOpenOption.TRUNCATE_EXISTING);
    }

    public static File getFileSettings(Path directory) {
        return new File(directory.toFile(), "game-manager.yml");
    }

    public void load() {
        this.players = new HashMap<>();
        this.arenas.forEach(Arena::enable);
        this.arenas.forEach(Arena::load);
    }

    public void tick() {
        this.arenas.stream().filter(arena -> !arena.isFree() && arena.isPlayable()).forEach(Arena::tick);
        if (this.arenas.isEmpty() || this.arenas.stream().allMatch(arena -> arena.getMaps().stream().noneMatch(map -> map.getSpawns().size() >= arena.getSettings().getTeamsNumber())))
            return;
        if (this.getArenas().stream().allMatch(arena -> arena.getGameState() == Arena.State.RESTARTING) && MatchBukkit.get().getCommandManager().getEditors().isEmpty())
            MatchBukkit.get().getServer().shutdown();
    }

    public void registerArena(Arena arena) {
        this.arenas.add(arena);
    }

    public List<Arena> getArenas() {
        return this.arenas;
    }

    public List<Arena> getFreeArenas() {
        return this.arenas.stream().filter(Arena::isFree).toList();
    }

    public List<Arena> getPlayableArenas() {
        return this.arenas.stream().filter(Arena::isPlayable).toList();
    }

    public List<Arena> getFreePlayableArenas() {
        return this.arenas.stream().filter(Arena::isFree).filter(Arena::isPlayable).toList();
    }

    public HashMap<UUID, GamePlayer> getPlayers() {
        return this.players;
    }

    public boolean isPlayer(UUID uniqueId) {
        return this.players.containsKey(uniqueId);
    }
}
