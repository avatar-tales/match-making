package fr.bakaaless.matchmaking.bukkit.lobby.listeners;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.lobby.leaderboard.LeaderBoard;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class Connection implements Listener {

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        if (MatchBukkit.get().getLobbyConfig().get("leaderboard.enabled", true)) {
            MatchBukkit.get().getServer().getScheduler().runTask(MatchBukkit.get(), () -> {
                try {
                    new LeaderBoard(event.getPlayer());
                } catch (ExecutionException | InterruptedException | TimeoutException e) {
                    throw new RuntimeException(e);
                }
            });
        }
    }

    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent event) {
        try {
            LeaderBoard.getLeaderBoards().remove(event.getPlayer()).destroy();
        } catch (Exception ignored) {}
    }
}
