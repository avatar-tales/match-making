package fr.bakaaless.matchmaking.bukkit.lobby.leaderboard;

public enum BoardMode {

    ELO,
    WINS,
    KILLS,

}
