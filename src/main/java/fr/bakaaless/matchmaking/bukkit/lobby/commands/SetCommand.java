package fr.bakaaless.matchmaking.bukkit.lobby.commands;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.lobby.config.Messages;
import fr.bakaaless.matchmaking.bukkit.lobby.leaderboard.LeaderBoard;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class SetCommand {

    public boolean run(final CommandSender sender, final List<String> arguments) {
        if (!sender.hasPermission(MatchBukkit.get().getLobbyConfig().get("permissions.edit", "matchmaking.leaderboard.modify"))) {
            ((Player) sender).performCommand("stats");
            return false;
        }
        try {
            LeaderBoard.setLocation(((Player) sender).getLocation());
            sender.sendMessage(Messages.INFO_LEADERBOARD_EDITED.get());
            if (MatchBukkit.get().getLobbyConfig().get("leaderboard.enabled", true)) {
                MatchBukkit.get().getServer().getOnlinePlayers().stream().map(LeaderBoard.getLeaderBoards()::get).forEach(LeaderBoard::destroy);
                MatchBukkit.get().getServer().getOnlinePlayers().forEach(player -> {
                    try {
                        new LeaderBoard(player);
                    } catch (ExecutionException | InterruptedException | TimeoutException e) {
                        throw new RuntimeException(e);
                    }
                });
            }
            return true;
        } catch (IOException e) {
            sender.sendMessage(Messages.ERROR_UNKNOWN.get());
            return false;
        }
    }
}
