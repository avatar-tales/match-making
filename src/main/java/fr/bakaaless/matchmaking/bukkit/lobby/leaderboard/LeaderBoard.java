package fr.bakaaless.matchmaking.bukkit.lobby.leaderboard;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.lobby.config.Messages;
import fr.bakaaless.matchmaking.bukkit.lobby.utils.InteractiveMenu;
import fr.bakaaless.matchmaking.common.satistics.Stats;
import io.papermc.paper.adventure.PaperAdventure;
import net.kyori.adventure.key.Key;
import net.kyori.adventure.sound.Sound;
import net.minecraft.network.protocol.game.ClientboundSetEntityDataPacket;
import net.minecraft.world.entity.decoration.ArmorStand;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_19_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class LeaderBoard extends InteractiveMenu {

    private static final WeakHashMap<Player, LeaderBoard> LEADER_BOARDS = new WeakHashMap<>();
    private final ArmorStand[]     decorations;
    private final List<ArmorStand> lines;
    private       int                    mode;
    public LeaderBoard(final Player player) throws ExecutionException, InterruptedException, TimeoutException {
        super(player);
        this.decorations = new ArmorStand[2];
        this.lines = new ArrayList<>();
        LEADER_BOARDS.put(player, this);
        this.show();
    }

    public static WeakHashMap<Player, LeaderBoard> getLeaderBoards() {
        return LEADER_BOARDS;
    }

    private void show() throws ExecutionException, InterruptedException, TimeoutException {
        this.mode = BoardMode.ELO.ordinal();
        final double line_space = MatchBukkit.get().getLobbyConfig().get("leaderboard.line_space", 0d);
        final Location location = this.getLocation();
        this.decorations[0] = summonArmorStand(Messages.INFO_LEADERBOARD_WAITING.getComponent(), location);
        location.add(0, line_space + 0.4, 0);
        for (int i = 0; i < 10; i++) {
            this.lines.add(summonArmorStand(Messages.INFO_LEADERBOARD_WAITING.getComponent(), location));
            location.add(0, line_space + 0.22, 0);
        }
        Collections.reverse(this.lines);
        this.decorations[1] = summonArmorStand(Messages.INFO_LEADERBOARD_TITLE.getComponent(), location.add(0, line_space + 0.4 - 0.22, 0));
        this.update();
    }

    public void destroy() {
        final List<ArmorStand> entities = new ArrayList<>(this.lines);
        Collections.addAll(entities, this.decorations);
        if (this.player.isOnline()) {
            super.uninject();
            entities.forEach(super::remove);
        }
        this.decorations[0] = null;
        this.decorations[1] = null;
        this.lines.clear();
    }

    private BoardMode getMode() {
        return BoardMode.values()[this.mode];
    }

    private void switchMode() {
        this.mode = BoardMode.values()[(this.mode + 1) % BoardMode.values().length].ordinal();
    }

    private void update() throws ExecutionException, InterruptedException, TimeoutException {
        this.lines.forEach(armorStand -> armorStand.setCustomName(PaperAdventure.asVanilla(Messages.INFO_LEADERBOARD_WAITING.getComponent())));
        final List<Stats> topUsers = BoardCache.getTopUsers(this.getMode());
        this.lines.forEach(armorStand -> armorStand.setCustomName(PaperAdventure.asVanilla(Messages.INFO_LEADERBOARD_EMPTY.getComponent())));
        for (int index = 0; index < topUsers.size(); index++) {
            final Stats user = topUsers.get(index);
            final ArmorStand armorStand = this.lines.get(index);
            int value = switch (this.getMode()) {
                case KILLS -> user.kills;
                case WINS -> user.wins;
                case ELO -> user.elo;
            };
            armorStand.setCustomName(PaperAdventure.asVanilla(Messages.INFO_LEADERBOARD_POSITION.getComponent(String.valueOf(index + 1), user.display + " &a" + user.name, String.valueOf(value))));
        }
        final Messages selector = Messages.INFO_LEADERBOARD_SELECTED;
        this.decorations[0].setCustomName(PaperAdventure.asVanilla(Messages.INFO_LEADERBOARD_SWITCH.getComponent((this.mode == BoardMode.ELO.ordinal() ? selector.get() : ""), (this.mode == BoardMode.WINS.ordinal() ? selector.get() : ""), (this.mode == BoardMode.KILLS.ordinal() ? selector.get() : ""))));
        final List<ArmorStand> toUpdate = new ArrayList<>(this.lines);
        toUpdate.add(this.decorations[0]);
        toUpdate.forEach(armorStand ->
                ((CraftPlayer) this.player).getHandle().connection.send(new ClientboundSetEntityDataPacket(armorStand.getId(), armorStand.getEntityData().getNonDefaultValues()))
        );
    }

    @Override
    public boolean interact(final int entityId) {
        if (this.lines.stream().noneMatch(armorStand -> armorStand.getId() == entityId))
            if (this.decorations[0].getId() != entityId && this.decorations[1].getId() != entityId)
                return false;
        this.switchMode();
        try {
            this.update();
            this.player.playSound(Sound.sound(Key.key("minecraft:ui.toast.in"), Sound.Source.PLAYER, 100, 1), this.getLocation().getX(), this.getLocation().getY(), this.getLocation().getZ());
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
        }
        return true;
    }

    public Location getLocation() {
        return new Location(Bukkit.getWorld(MatchBukkit.get().getLobbyConfig().get("leaderboard.position.world", "")), MatchBukkit.get().getLobbyConfig().get("leaderboard.position.x", 0D), MatchBukkit.get().getLobbyConfig().get("leaderboard.position.y", 0D), MatchBukkit.get().getLobbyConfig().get("leaderboard.position.z", 0D));
    }

    public static void setLocation(final Location location) throws IOException {
        MatchBukkit.get().getLobbyConfig().write("leaderboard.position.world", location.getWorld().getName());
        MatchBukkit.get().getLobbyConfig().write("leaderboard.position.x", location.getX());
        MatchBukkit.get().getLobbyConfig().write("leaderboard.position.y", location.getY());
        MatchBukkit.get().getLobbyConfig().write("leaderboard.position.z", location.getZ());
        MatchBukkit.get().getLobbyConfig().save();
    }

}
