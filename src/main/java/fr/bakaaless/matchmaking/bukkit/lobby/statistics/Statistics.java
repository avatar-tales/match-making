package fr.bakaaless.matchmaking.bukkit.lobby.statistics;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.datafixers.util.Pair;
import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.lobby.config.Messages;
import fr.bakaaless.matchmaking.bukkit.lobby.utils.InteractiveMenu;
import fr.bakaaless.matchmaking.bukkit.lobby.utils.ReflectionUtils;
import net.minecraft.network.protocol.game.ClientboundSetEquipmentPacket;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.decoration.ArmorStand;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_19_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_19_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.util.Vector;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.WeakHashMap;

public class Statistics extends InteractiveMenu {

    private static final WeakHashMap<Player, Statistics> STATISTICS = new WeakHashMap<>();
    private final List<ArmorStand> stats;
    private final User                   lookup;
    private ArmorStand title;
    private       ArmorStand       close;
    public Statistics(final Player player, final User lookup) {
        super(player);
        this.stats = new ArrayList<>();
        this.lookup = lookup;
        STATISTICS.put(player, this);
        this.show();
    }

    public static WeakHashMap<Player, Statistics> getStatistics() {
        return STATISTICS;
    }

    public void show() {
        final Location location = this.player.getLocation();
        final float initialYaw = location.getYaw();
        final double radius = MatchBukkit.get().getLobbyConfig().getOrWrite("statistics.radius", 2.0D);

        this.title = this.summonArmorStand(Messages.INFO_STATISTICS_TITLE.getComponent("&l" + this.lookup.display.toUpperCase() + " &b&l" + this.lookup.name.toUpperCase()), location.clone().add(vector(initialYaw, 0, radius + 0.3)).add(0, 1.5, 0));

        this.close = this.summonArmorStand(Messages.INFO_STATISTICS_CLOSE_TITLE.getComponent(), location.clone().add(vector(initialYaw, -75, radius)).add(0, 1.7, 0), true);
        this.setupMaterial(this.close, Messages.INFO_STATISTICS_CLOSE_ITEM);

        final String[] stats = {"ELO", "KILLS", "DEATHS", "WINS", "GAMES"};
        ArmorStand entity;

        for (int index = 0; index < stats.length; index++) {
            entity = this.summonArmorStand(Messages.valueOf("INFO_STATISTICS_" + stats[index] + "_TITLE").getComponent(String.valueOf(ReflectionUtils.readField(this.lookup, stats[index].toLowerCase())), String.valueOf(this.lookup.ranks[index % 5])), location.clone().add(vector(initialYaw, -100 + (index) * 240f / stats.length, radius + 0.5)).add(0, 0.2 * Math.pow(-1, index), 0), true);
            this.setupMaterial(entity, Messages.valueOf("INFO_STATISTICS_" + stats[index] + "_ITEM"));
            this.stats.add(entity);
        }
    }

    private Vector vector(final float yaw, final float angle, final double amplifier) {
        final double theta = (yaw + angle + 90) * Math.PI / 180;
        return new Vector(Math.cos(theta), 0, Math.sin(theta)).multiply(amplifier);
    }

    private void setupMaterial(final ArmorStand entity, final Messages message) {
        final String[] closeMaterial = message.getFullRaw().split(":");
        final Material material = Material.valueOf(closeMaterial[0]);
        ItemStack item;
        if (material.name().contains("PLAYER_HEAD")) {
            item = new ItemStack(Material.PLAYER_HEAD, 1);
            final SkullMeta itemMeta = (SkullMeta) item.getItemMeta();
            try {
                GameProfile gameProfile = new GameProfile(UUID.randomUUID(), null);
                gameProfile.getProperties().put("textures", new Property("textures", closeMaterial[1]));
                assert itemMeta != null;
                final Field declaredField = itemMeta.getClass().getDeclaredField("profile");
                declaredField.setAccessible(true);
                declaredField.set(itemMeta, gameProfile);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
            item.setItemMeta(itemMeta);
        } else {
            item = new ItemStack(material, 1);
        }
        this.equip(entity, item, org.bukkit.inventory.EquipmentSlot.HEAD);
    }

    private void equip(final ArmorStand object, final ItemStack item, final org.bukkit.inventory.EquipmentSlot slot) {
        final List<Pair<EquipmentSlot, net.minecraft.world.item.ItemStack>> list = new ArrayList<>();
        list.add(Pair.of(EquipmentSlot.values()[slot.ordinal()], CraftItemStack.asNMSCopy(item)));
        ((CraftPlayer) this.player).getHandle().connection.send(new ClientboundSetEquipmentPacket(object.getId(), list));
    }

    public void destroy() {
        if (this.player.isOnline()) {
            this.uninject();
            final List<ArmorStand> toDelete = this.stats;
            toDelete.add(this.title);
            toDelete.add(this.close);
            toDelete.forEach(super::remove);
        }
    }

    @Override
    public boolean interact(int entityId) {
        if (this.close.getId() == entityId) {
            this.destroy();
            return true;
        }
        return false;
    }
}
