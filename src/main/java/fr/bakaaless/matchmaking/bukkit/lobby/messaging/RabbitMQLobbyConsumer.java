package fr.bakaaless.matchmaking.bukkit.lobby.messaging;

import fr.bakaaless.matchmaking.bukkit.common.messaging.RabbitMQBukkitConsumer;
import fr.bakaaless.matchmaking.bukkit.lobby.leaderboard.BoardMode;
import fr.bakaaless.matchmaking.bukkit.lobby.statistics.User;
import fr.bakaaless.matchmaking.common.messaging.Message;
import fr.bakaaless.matchmaking.common.messaging.messages.lobby.StatsResponse;
import fr.bakaaless.matchmaking.common.messaging.messages.lobby.TopPlayerResponse;
import fr.bakaaless.matchmaking.common.messaging.rabbitmq.RabbitMQ;
import fr.bakaaless.matchmaking.common.satistics.Stats;

import java.util.*;
import java.util.concurrent.CompletableFuture;

public class RabbitMQLobbyConsumer extends RabbitMQBukkitConsumer {

    private final Map<BoardMode, Queue<CompletableFuture<List<Stats>>>> completeTopPlayers = new HashMap<>();
    private final Map<String, Queue<CompletableFuture<User>>>           completeUser       = new HashMap<>();

    public RabbitMQLobbyConsumer(RabbitMQ rabbitMQ) {
        super(rabbitMQ);
    }

    @Override
    public void accept(Message message) {
        super.accept(message);
        if (message.type() == Message.MessageType.TOP_PLAYERS_RESPONSE) {
            TopPlayerResponse packet = super.instance.getGson().fromJson(message.content(), TopPlayerResponse.class);
            BoardMode mode = BoardMode.valueOf(packet.getMode().toUpperCase());
            Queue<CompletableFuture<List<Stats>>> queue = this.completeTopPlayers.get(mode);
            if (queue == null)
                return;
            CompletableFuture<List<Stats>> completableFuture = queue.poll();
            if (completableFuture == null) {
                this.completeTopPlayers.remove(mode);
                return;
            }
            completableFuture.complete(packet.getResult());
            if (queue.isEmpty())
                this.completeTopPlayers.remove(mode);
        }
        if (message.type() == Message.MessageType.STATS_RESPONSE) {
            StatsResponse packet = super.instance.getGson().fromJson(message.content(), StatsResponse.class);
            Queue<CompletableFuture<User>> queue = this.completeUser.get(packet.getId());
            if (queue == null)
                return;
            CompletableFuture<User> completableFuture = queue.poll();
            if (completableFuture == null) {
                this.completeUser.remove(packet.getId());
                return;
            }
            completableFuture.complete(new User(packet.getStats()));
            if (queue.isEmpty())
                this.completeUser.remove(packet.getId());
        }
    }

    public void requestTopPlayer(CompletableFuture<List<Stats>> toComplete, BoardMode mode) {
        this.completeTopPlayers.putIfAbsent(mode, new LinkedList<>());
        this.completeTopPlayers.get(mode).add(toComplete);
    }

    public void requestUser(CompletableFuture<User> toComplete, String name) {
        this.completeUser.putIfAbsent(name, new LinkedList<>());
        this.completeUser.get(name).add(toComplete);
    }
}
