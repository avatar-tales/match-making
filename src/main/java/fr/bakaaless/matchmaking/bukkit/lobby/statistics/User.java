package fr.bakaaless.matchmaking.bukkit.lobby.statistics;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.lobby.messaging.RabbitMQLobbyConsumer;
import fr.bakaaless.matchmaking.common.messaging.Message;
import fr.bakaaless.matchmaking.common.messaging.messages.lobby.StatsRequest;
import fr.bakaaless.matchmaking.common.satistics.Stats;

import java.util.UUID;
import java.util.WeakHashMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class User extends Stats {

    private static final WeakHashMap<String, User> CACHE = new WeakHashMap<>();

    private final long expiration;

    public User(String uniqueId) {
        super(uniqueId);
        this.expiration = 0L;

    }

    public User(Stats stats) {
        this(stats.uniqueId, stats.display, stats.name, stats.elo, stats.kills, stats.deaths, stats.wins, stats.games, stats.ranks);
    }

    public User(UUID uniqueId, String display, String name, int elo, int kills, int deaths, int wins, int games, int[] ranks) {
        super(uniqueId, display, name, elo, kills, deaths, wins, games, ranks);
        this.expiration = System.currentTimeMillis() + MatchBukkit.get().getLobbyConfig().get("cache.expiration", 300000);
        CACHE.put(name, this);
    }

    public static User getUser(final String uniqueId) throws ExecutionException, InterruptedException, TimeoutException {
        if (!MatchBukkit.get().getLobbyConfig().get("cache.enabled", true))
            return get(uniqueId);
        if (!CACHE.containsKey(uniqueId) || CACHE.get(uniqueId).expiration < System.currentTimeMillis()) {
            CACHE.remove(uniqueId);
            CACHE.put(uniqueId, get(uniqueId));
        }
        return CACHE.get(uniqueId);
    }

    private static User get(final String uniqueId) throws ExecutionException, InterruptedException, TimeoutException {
        CompletableFuture<User> completableFuture = new CompletableFuture<>();
        ((RabbitMQLobbyConsumer) MatchBukkit.get().getRabbitMQ().getConsumer()).requestUser(completableFuture, uniqueId);
        MatchBukkit.get().sendPacket(new StatsRequest(uniqueId), Message.MessageType.STATS_REQUEST);
        return completableFuture.get(1000, TimeUnit.MILLISECONDS);
    }


}
