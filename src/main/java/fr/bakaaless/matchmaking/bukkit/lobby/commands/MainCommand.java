package fr.bakaaless.matchmaking.bukkit.lobby.commands;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.lobby.statistics.Statistics;
import fr.bakaaless.matchmaking.bukkit.lobby.statistics.User;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class MainCommand implements CommandExecutor, TabCompleter {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;
        final List<String> arguments = new ArrayList<>(Arrays.asList(args));
        final String module = arguments.size() > 0 ? arguments.remove(0) : "";
        if (module.equalsIgnoreCase("set"))
            return new SetCommand().run(sender, arguments);
        else {
            final Player player = (Player) sender;
            if (Statistics.getStatistics().containsKey(player))
                Statistics.getStatistics().remove(player).destroy();
            try {
                new Statistics(player, User.getUser(!module.isEmpty() && player.hasPermission(MatchBukkit.get().getLobbyConfig().get("permissions.stats", "matchmaking.statistics.modify")) ? module : player.getUniqueId().toString()));
            } catch (ExecutionException | InterruptedException | TimeoutException e) {
                throw new RuntimeException(e);
            }
            return true;
        }
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (!(sender instanceof Player))
            return new ArrayList<>();
        final List<String> completer = new ArrayList<>();
        if (!sender.hasPermission(MatchBukkit.get().getLobbyConfig().get("permissions.stats", "matchmaking.statistics.other"))) {
            if (sender.getName().startsWith(args[0]))
                completer.add(sender.getName());
        } else {
            for (final Player player : MatchBukkit.get().getServer().getOnlinePlayers()) {
                if (player.getName().startsWith(args[0]))
                    completer.add(player.getName());
            }
        }
        if (sender.hasPermission(MatchBukkit.get().getLobbyConfig().get("permissions.edit", "matchmaking.leaderboard.modify")))
            if ("set".startsWith(args[0]))
                completer.add("set");
        return completer;
    }
}
