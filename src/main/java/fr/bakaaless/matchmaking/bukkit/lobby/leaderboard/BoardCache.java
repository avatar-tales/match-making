package fr.bakaaless.matchmaking.bukkit.lobby.leaderboard;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import fr.bakaaless.matchmaking.bukkit.lobby.messaging.RabbitMQLobbyConsumer;
import fr.bakaaless.matchmaking.common.messaging.Message;
import fr.bakaaless.matchmaking.common.messaging.messages.lobby.TopPlayersRequest;
import fr.bakaaless.matchmaking.common.satistics.Stats;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class BoardCache {

    private final static Map<BoardMode, BoardCache> CACHE = new HashMap<>();
    long        expiration;
    List<Stats> topUsers;

    public BoardCache(final long expiration, final List<Stats> topUsers) {
        this.expiration = expiration;
        this.topUsers = topUsers;
    }

    public static List<Stats> getTopUsers(final BoardMode mode) throws ExecutionException, InterruptedException, TimeoutException {
        if (!MatchBukkit.get().getLobbyConfig().get("cache.enabled", true))
            return getTopPlayers(mode, 10);
        if (!CACHE.containsKey(mode) || CACHE.get(mode).expiration < System.currentTimeMillis() || CACHE.get(mode).topUsers.size() == 0) {
            CACHE.remove(mode);
            CACHE.put(mode, new BoardCache(System.currentTimeMillis() + MatchBukkit.get().getLobbyConfig().get("cache.expiration", 300000), getTopPlayers(mode, 10)));
        }
        return CACHE.get(mode).topUsers;
    }

    private static List<Stats> getTopPlayers(BoardMode mode, int limit) throws ExecutionException, InterruptedException, TimeoutException {
        CompletableFuture<List<Stats>> completableFuture = new CompletableFuture<>();
        ((RabbitMQLobbyConsumer) MatchBukkit.get().getRabbitMQ().getConsumer()).requestTopPlayer(completableFuture, mode);
        MatchBukkit.get().sendPacket(new TopPlayersRequest(mode.name(), limit), Message.MessageType.TOP_PLAYERS_REQUEST);
        return completableFuture.get(1000, TimeUnit.MILLISECONDS);
    }

}
