package fr.bakaaless.matchmaking.bukkit.lobby.utils;

import fr.bakaaless.matchmaking.bukkit.MatchBukkit;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.papermc.paper.adventure.PaperAdventure;
import net.kyori.adventure.text.Component;
import net.minecraft.network.protocol.game.ClientboundAddEntityPacket;
import net.minecraft.network.protocol.game.ClientboundRemoveEntitiesPacket;
import net.minecraft.network.protocol.game.ClientboundSetEntityDataPacket;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.network.ServerGamePacketListenerImpl;
import net.minecraft.world.entity.decoration.ArmorStand;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_19_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_19_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.List;

public abstract class InteractiveMenu {

    protected final Player player;
    protected final long startTime;
    private Channel channel;

    private long rateLimit;

    public InteractiveMenu(Player player) {
        this.player = player;
        this.startTime = System.currentTimeMillis();
        this.rateLimit = System.currentTimeMillis() + MatchBukkit.get().getLobbyConfig().get("rate_limit.duration", 500);
        this.inject();
    }

    protected ArmorStand summonArmorStand(final Component name, final Location origin, final boolean lookingPlayer) {
        Location location = origin.clone().add(this.player.getLocation().getDirection().normalize().multiply(0.9));
        ServerLevel worldServer = ((CraftWorld) origin.getWorld()).getHandle();
        double x = location.getX();
        double y = location.getY();
        double z = location.getZ();
        if (lookingPlayer) {
            Vector vector = this.player.getLocation().toVector().subtract(origin.toVector());
            location.setDirection(vector);
        }
        float yaw = location.getYaw();
        float pitch = location.getPitch();

        ArmorStand entity = new ArmorStand(worldServer, x, y, z);
        entity.teleportTo(x, y, z);
        entity.setRot(yaw, pitch);
        entity.setCustomName(PaperAdventure.asVanilla(name));
        entity.setCustomNameVisible(true);
        entity.setInvisible(true);
        entity.setSmall(true);

        ServerGamePacketListenerImpl playerConnection = ((CraftPlayer) this.player).getHandle().connection;

        playerConnection.send(new ClientboundAddEntityPacket(entity));
        playerConnection.send(new ClientboundSetEntityDataPacket(entity.getId(), entity.getEntityData().getNonDefaultValues()));
        return entity;
    }

    protected ArmorStand summonArmorStand(final Component name, final Location origin) {
        return this.summonArmorStand(name, origin, false);
    }

    private void inject() {
        this.channel = ((CraftPlayer) this.player).getHandle().connection.connection.channel;

        if (this.channel.pipeline().get("PacketInjector" + this.startTime) != null)
            return;

        decode(ReflectionUtils.getPacketClass("PacketPlayInUseEntity"));
    }

    protected void uninject() {
        if (this.channel.pipeline().get("PacketInjector" + this.startTime) != null)
            this.channel.pipeline().remove("PacketInjector" + this.startTime);
    }

    protected void remove(final ArmorStand entity) {
        ((CraftPlayer) this.player).getHandle().connection.send(new ClientboundRemoveEntitiesPacket(entity.getId()));
    }

    private <T> void decode(final Class<T> packetClass) {
        this.channel.pipeline().addAfter("decoder", "PacketInjector" + this.startTime,
                new MessageToMessageDecoder<T>() {
                    @Override
                    protected void decode(final ChannelHandlerContext channelHandlerContext, final T packet, final List<Object> arg) {
                        if (!reader(packet))
                            arg.add(packet);
                    }
                }
        );
    }

    public abstract boolean interact(final int entityId);

    public <T> boolean reader(final T packet) {
        if (!packet.getClass().getSimpleName().equals("PacketPlayInUseEntity"))
            return false;
        final String action = (ReflectionUtils.MINECRAFT_VERSION < 17 ? ReflectionUtils.readField(packet, "action") : ReflectionUtils.executeMethod(ReflectionUtils.readField(packet, "b"), "a")).toString();

        if (action.equalsIgnoreCase("ATTACK") || action.equalsIgnoreCase("INTERACT_AT")) {
            final int entityId = (int) ReflectionUtils.readField(packet, "a");

            if (interact(entityId)) {
                if (MatchBukkit.get().getLobbyConfig().get("rate_limit.enabled", true) && System.currentTimeMillis() < this.rateLimit)
                    return true;
                this.rateLimit = System.currentTimeMillis() + MatchBukkit.get().getLobbyConfig().get("rate_limit.duration", 500);
                return true;
            }
        }
        return false;
    }

}
