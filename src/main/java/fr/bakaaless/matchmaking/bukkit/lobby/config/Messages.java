package fr.bakaaless.matchmaking.bukkit.lobby.config;

import fr.bakaaless.matchmaking.common.configuration.Config;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import net.md_5.bungee.api.ChatColor;

import java.nio.file.Path;

public enum Messages {

    PREFIX_INFO("&3Match Making &9»"),
    PREFIX_ERROR("&cMatch Making &4»"),

    INFO_LEADERBOARD_EDITED("%prefix_info% &7Le tableau du classement a bien été mis à jour."),

    INFO_LEADERBOARD_TITLE("&b&lCLASSEMENT 1v1 RANKED"),
    INFO_LEADERBOARD_WAITING("&7Chargement..."),
    INFO_LEADERBOARD_EMPTY("&7Vide"),
    INFO_LEADERBOARD_POSITION("&e{0}. &7{1} - &e{2}"),
    INFO_LEADERBOARD_SWITCH("&7{0}Élo &7{1}Victoires &7{2}Kills"),
    INFO_LEADERBOARD_SELECTED("&a&l"),

    INFO_STATISTICS_TITLE("&b&lSTATISTIQUES DE {0}"),
    INFO_STATISTICS_CLOSE_TITLE("&cFermer"),
    INFO_STATISTICS_CLOSE_ITEM("PLAYER_HEAD:eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWE2Nzg3YmEzMjU2NGU3YzJmM2EwY2U2NDQ5OGVjYmIyM2I4OTg0NWU1YTY2YjVjZWM3NzM2ZjcyOWVkMzcifX19"),
    INFO_STATISTICS_KILLS_TITLE("&c{0} kills | Rang {1}"),
    INFO_STATISTICS_KILLS_ITEM("PLAYER_HEAD:eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZThiZTM5N2NhZmJjZjVmYThjYjRjZDc1YmI2MTc0MGIzZWRlN2FjMWNlN2Q3Njc1ZDkwOTNiMTZkZWIzMSJ9fX0="),
    INFO_STATISTICS_DEATHS_TITLE("&c{0} morts | Rang {1}"),
    INFO_STATISTICS_DEATHS_ITEM("PLAYER_HEAD:eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMWUyYzc1YzE4MjJmOGY2ZWZhOWY0NTRhOTRjZjIyOWE2ZDAyYjM5MWEzZTYwNjFkZGMyMTI2N2E1N2RmZjUifX19"),
    INFO_STATISTICS_ELO_TITLE("&c{0} elo | Rang {1}"),
    INFO_STATISTICS_ELO_ITEM("PLAYER_HEAD:eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWMxYTdlMTkzZjM3YzJjNTRlMzU4ZjIyYThhMmQwMjg5NzkzZGQzYjJkNmM3OTllODQyNGI5MjZhMzk1MSJ9fX0="),
    INFO_STATISTICS_WINS_TITLE("&c{0} victoires | Rang {1}"),
    INFO_STATISTICS_WINS_ITEM("PLAYER_HEAD:eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTQ1ZjQ3ZmViNGQ3NWNiMzMzOTE0YmZkYjk5OWE0ODljOWQwZTMyMGQ1NDhmMzEwNDE5YWQ3MzhkMWUyNGI5In19fQ=="),
    INFO_STATISTICS_GAMES_TITLE("&c{0} parties | Rang {1}"),
    INFO_STATISTICS_GAMES_ITEM("PLAYER_HEAD:eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGQxOWM2ODQ2MTY2NmFhY2Q3NjI4ZTM0YTFlMmFkMzlmZTRmMmJkZTMyZTIzMTk2M2VmM2IzNTUzMyJ9fX0="),

    ERROR_UNKNOWN("%prefix_error% &cQuelque chose s'est mal passé..."),
    ;

    private String message;

    Messages(final String message) {
        this.message = message;
    }

    public static void init(final String path, final String name) {
        try {
            final Config config = Config.load(Path.of(path), name);
            for (final Messages messages : Messages.values()) {
                final String pathToMessage = messages.name().replace("__", "-").replace("_", ".").toLowerCase();
                messages.message = config.getOrWrite(pathToMessage, messages.message);
            }
            config.save();
        } catch (Exception ignored) {
        }
    }

    public String getFullRaw() {
        return this.message;
    }

    public String get(final String... arguments) {
        String result = this.message;
        for (int i = 0; i < arguments.length; i++)
            result = result.replace("{" + i + "}", arguments[i]);
        return ChatColor.translateAlternateColorCodes('&', result
                .replace("%prefix_info%", PREFIX_INFO.message)
                .replace("%prefix_error%", PREFIX_ERROR.message)
                .replace("¤", " ")
        );
    }

    public Component getComponent(final String... arguments) {
        String result = this.message;
        for (int i = 0; i < arguments.length; i++)
            result = result.replace("{" + i + "}", arguments[i]);
        return LegacyComponentSerializer.legacyAmpersand().deserialize(result
                .replace("%prefix_info%", PREFIX_INFO.message)
                .replace("%prefix_error%", PREFIX_ERROR.message)
                .replace("¤", " ")
        );
    }

    public String[] getSplit(final String... arguments) {
        return this.get(arguments).split("%nl%");
    }

}
