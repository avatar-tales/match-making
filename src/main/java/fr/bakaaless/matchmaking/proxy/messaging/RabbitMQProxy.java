package fr.bakaaless.matchmaking.proxy.messaging;

import fr.bakaaless.matchmaking.common.games.ArenaSettings;
import fr.bakaaless.matchmaking.common.games.Rank;
import fr.bakaaless.matchmaking.common.messaging.Message;
import fr.bakaaless.matchmaking.common.messaging.messages.game.SendGroup;
import fr.bakaaless.matchmaking.common.messaging.messages.other.RequestBending;
import fr.bakaaless.matchmaking.common.messaging.rabbitmq.RabbitMQ;
import fr.bakaaless.matchmaking.proxy.MatchPlugin;
import fr.bakaaless.matchmaking.proxy.data.ClusterServer;
import fr.bakaaless.matchmaking.proxy.data.MatchUser;
import fr.bakaaless.matchmaking.proxy.process.WaitingUser;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

public class RabbitMQProxy extends RabbitMQ {

    public RabbitMQProxy(final Path directory) {
        super(directory);
    }

    public void sendGroup(final WaitingUser[] users, final ClusterServer server, final ArenaSettings arena, boolean ranked, int pointsToWin) throws IOException, TimeoutException {
        UUID[] players = Arrays.stream(users)
                .map(WaitingUser::getUser)
                .map(MatchUser::getUniqueId)
                .toArray(UUID[]::new);
        Map<UUID, Rank> ranks = new HashMap<>();
        Arrays.stream(users).forEach(waitingUser -> ranks.put(waitingUser.getUser().getUniqueId(), new Rank(MatchPlugin.getInstance().getRank(waitingUser.getUser().getElo()), waitingUser.getUser().getElo())));
        final SendGroup sendGroup = new SendGroup(players, ranks, arena, ranked, pointsToWin);
        MatchPlugin.getInstance().getRabbitMQ().sendMessage(new Message(server.getName(), "proxy", Message.MessageType.SEND_GROUP, this.getGson().toJson(sendGroup)));
    }

    public void requestBend(UUID player) throws IOException, TimeoutException {
        this.sendMessage(new Message("*", "proxy", Message.MessageType.REQUEST_BENDING, this.getGson().toJson(new RequestBending(player))));
    }

}
