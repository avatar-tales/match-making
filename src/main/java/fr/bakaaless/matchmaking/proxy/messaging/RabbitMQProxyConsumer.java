package fr.bakaaless.matchmaking.proxy.messaging;

import com.velocitypowered.api.proxy.Player;
import fr.bakaaless.matchmaking.common.databases.HikariConnection;
import fr.bakaaless.matchmaking.common.messaging.Message;
import fr.bakaaless.matchmaking.common.messaging.messages.PongPacket;
import fr.bakaaless.matchmaking.common.messaging.messages.game.End;
import fr.bakaaless.matchmaking.common.messaging.messages.game.Event;
import fr.bakaaless.matchmaking.common.messaging.messages.game.RemoveSpectators;
import fr.bakaaless.matchmaking.common.messaging.messages.game.StartGame;
import fr.bakaaless.matchmaking.common.messaging.messages.lobby.*;
import fr.bakaaless.matchmaking.common.messaging.messages.other.HasBending;
import fr.bakaaless.matchmaking.common.messaging.rabbitmq.RabbitMQ;
import fr.bakaaless.matchmaking.common.messaging.rabbitmq.RabbitMQConsumer;
import fr.bakaaless.matchmaking.common.satistics.Stats;
import fr.bakaaless.matchmaking.proxy.MatchPlugin;
import fr.bakaaless.matchmaking.proxy.config.MatchMessages;
import fr.bakaaless.matchmaking.proxy.data.ClusterServer;
import fr.bakaaless.matchmaking.proxy.data.LobbyServer;
import fr.bakaaless.matchmaking.proxy.data.MatchUser;
import fr.bakaaless.matchmaking.proxy.data.Server;
import fr.bakaaless.proxycore.config.Messages;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import net.kyori.adventure.title.Title;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeoutException;

public class RabbitMQProxyConsumer extends RabbitMQConsumer {

    private final String request = """
            SELECT * FROM (
                SELECT uuid,
                       name,
                       elo,
                       IFNULL(games, 0) as games,
                       IFNULL(victories, 0) as wins,
                       IFNULL(kills, 0) as kills,
                       IFNULL(deaths, 0) as deaths,
                       RANK() OVER (ORDER BY elo DESC) as elo_rank,
                       RANK() OVER (ORDER BY games DESC) as games_rank,
                       RANK() OVER (ORDER BY victories DESC) as wins_rank,
                       RANK() OVER (ORDER BY kills DESC) as kills_rank,
                       RANK() OVER (ORDER BY deaths DESC) as deaths_rank
                FROM players
                LEFT JOIN (
                    SELECT player, COUNT(*) as games FROM playing GROUP BY player
                ) as Game
                ON uuid = Game.player
                LEFT JOIN (
                    SELECT player, COUNT(*) as victories FROM playing WHERE status = 2 GROUP BY player
                ) as Victory
                ON uuid = Victory.player
                LEFT JOIN (
                    SELECT player, COUNT(*) as kills FROM events WHERE action = 'KILL' GROUP BY player
                ) as Kill
                ON uuid = Kill.player
                LEFT JOIN (
                    SELECT player, COUNT(*) as deaths FROM events WHERE action = 'DEATH' GROUP BY player
                ) as Death
                ON uuid = Death.player
            )""";

    public RabbitMQProxyConsumer(final RabbitMQ rabbitMQ) {
        super(rabbitMQ);
    }

    @Override
    public void accept(final Message message) {
        switch (message.type()) {
            case PONG -> {
                final PongPacket packet = super.instance.getGson().fromJson(message.content(), PongPacket.class);
                MatchPlugin.getInstance().getServer().getAllServers().forEach(registeredServer -> {
                    if (registeredServer.getServerInfo().getAddress().getPort() == packet.getPort()) {
                        final List<Server> servers = ClusterServer.getServersByServer(registeredServer);
                        if (servers.isEmpty()) {
                            if (packet.isLobby())
                                Server.registerServer(new LobbyServer(registeredServer, message.source()));
                            else
                                Server.registerServer(new ClusterServer(message.source(), registeredServer, packet.getArenas()));
                        } else
                            servers.forEach(server -> {
                                server.setName(message.source());
                                if (server instanceof ClusterServer clusterServer)
                                    clusterServer.setArenas(packet.getArenas());
                                server.setLastPing(System.currentTimeMillis());
                            });
                    }
                });
            }
            case ADD_QUEUE -> {
                PlayerQueuePacket addPlayerQueue = super.instance.getGson().fromJson(message.content(), PlayerQueuePacket.class);
                MatchPlugin.getInstance().getServer().getPlayer(addPlayerQueue.getUser()).ifPresent(player ->
                        MatchPlugin.getInstance().getServer().getCommandManager().executeAsync(player, "matchmaking join")
                );
            }
            case REMOVE_QUEUE -> {
                PlayerQueuePacket removePlayerQueue = super.instance.getGson().fromJson(message.content(), PlayerQueuePacket.class);
                MatchPlugin.getInstance().getServer().getPlayer(removePlayerQueue.getUser()).ifPresent(player ->
                        MatchPlugin.getInstance().getServer().getCommandManager().executeAsync(player, "matchmaking leave")
                );
            }
            case END -> {
                End packet = super.instance.getGson().fromJson(message.content(), End.class);
                List<MatchUser> winners = Arrays.stream(packet.getVictory()).map(MatchUser::getUser).filter(Optional::isPresent).map(Optional::get).toList();
                List<MatchUser> losers = Arrays.stream(packet.getDefeat()).map(MatchUser::getUser).filter(Optional::isPresent).map(Optional::get).toList();
                List<Player> spectators = Arrays.stream(packet.getSpectators()).map(MatchPlugin.getInstance().getServer()::getPlayer).filter(Optional::isPresent).map(Optional::get).toList();
                if (packet.getReason() == End.EndReason.LEGIT && packet.isRanked()) {
                    int winnersAverageElo = (int) Math.round(winners.stream().mapToInt(MatchUser::getElo).summaryStatistics().getAverage());
                    int losersAverageElo = (int) Math.round(losers.stream().mapToInt(MatchUser::getElo).summaryStatistics().getAverage());

                    int winBaseElo = MatchPlugin.getInstance().getConfig().getOrWrite("match_making.elo.win", 10);
                    int loseBaseElo = MatchPlugin.getInstance().getConfig().getOrWrite("match_making.elo.defeat", 10);
                    double curves = MatchPlugin.getInstance().getConfig().getOrWrite("match_making.elo.curves", 0.03);

                    int difference = winnersAverageElo - losersAverageElo;
                    MatchPlugin.getInstance().getServer().getScheduler().buildTask(MatchPlugin.getInstance(), () -> {
                        winners.forEach(winner -> {
                            int winningElo = this.eloToWin(difference, winBaseElo, curves);
                            winner.setElo(winner.getElo() + winningElo);
                            winner.getPlayer().ifPresent(player ->
                                    Messages.sendMessage(player, MatchMessages.ELO_WIN.display(), String.valueOf(winningElo), player.getUsername())
                            );
                        });
                        losers.forEach(loser -> {
                            int losingElo = this.eloToWin(difference, loseBaseElo, curves);
                            loser.setElo(loser.getElo() - losingElo);
                            loser.getPlayer().ifPresent(player ->
                                    Messages.sendMessage(player, MatchMessages.ELO_LOSE.display(), String.valueOf(losingElo), player.getUsername())
                            );
                        });
                    }).delay(Duration.ofSeconds(3)).schedule();
                    new Thread(() -> {
                        HikariConnection hikariConnection = MatchPlugin.getInstance().getHikariCP();
                        winners.forEach(winner -> {
                            hikariConnection.doQuery("UPDATE `players` SET `elo` = '" + winner.getElo() + "' WHERE `uuid` = '" + winner.getUniqueId() + "'");
                            hikariConnection.doQuery("UPDATE `playing` SET `status` = '2' WHERE `game` = '" + packet.getGameId() + "' AND `player` = '" + winner.getUniqueId() + "';");
                        });
                        losers.forEach(loser -> {
                            hikariConnection.doQuery("UPDATE `players` SET `elo` = '" + loser.getElo() + "' WHERE `uuid` = '" + loser.getUniqueId() + "'");
                            hikariConnection.doQuery("UPDATE `playing` SET `status` = '1' WHERE `game` = '" + packet.getGameId() + "' AND `player` = '" + loser.getUniqueId() + "';");
                        });
                    }).start();
                } else if (packet.isRanked()) {

                    MatchPlugin.getInstance().getServer().getScheduler().buildTask(MatchPlugin.getInstance(), () -> {
                        winners.forEach(winner ->
                                winner.getPlayer().ifPresent(player ->
                                        Messages.sendMessage(player, MatchMessages.ELO_STABLE.display()
                                        )
                                )
                        );
                        losers.forEach(winner ->
                                winner.getPlayer().ifPresent(player ->
                                        Messages.sendMessage(player, MatchMessages.ELO_STABLE.display()
                                        )
                                )
                        );
                    }).delay(Duration.ofSeconds(3)).schedule();
                }
                Title victory = Title.title(
                        LegacyComponentSerializer.legacyAmpersand().deserialize("&6&lVICTOIRE"),
                        Component.text(""),
                        Title.Times.times(
                                Duration.ofMillis(50),
                                Duration.ofSeconds(7),
                                Duration.ofMillis(50)
                        )
                );
                Title defeat = Title.title(
                        LegacyComponentSerializer.legacyAmpersand().deserialize("&c&lDÉFAITE"),
                        Component.text(""),
                        Title.Times.times(
                                Duration.ofMillis(50),
                                Duration.ofSeconds(7),
                                Duration.ofMillis(50)
                        )
                );
                Server.getLobbies().stream().findAny().ifPresentOrElse(lobby -> {
                    finishPlayers(winners, victory, lobby);
                    finishPlayers(losers, defeat, lobby);
                    finishPlayers(spectators, lobby);
                }, () -> {
                    winners.forEach(matchUser -> {
                        matchUser.setPlaying(false);
                        matchUser.setRanked(true);
                        MatchPlugin.getInstance().getServer().getScheduler().buildTask(MatchPlugin.getInstance(), () ->
                                matchUser.getPlayer().ifPresent(player -> player.showTitle(victory))
                        ).delay(Duration.ofSeconds(3)).schedule();
                    });
                    losers.forEach(matchUser -> {
                        matchUser.setPlaying(false);
                        matchUser.setRanked(true);
                        MatchPlugin.getInstance().getServer().getScheduler().buildTask(MatchPlugin.getInstance(), () ->
                                matchUser.getPlayer().ifPresent(player -> player.showTitle(defeat))
                        ).delay(Duration.ofSeconds(3)).schedule();
                    });
                });
            }
            case EVENT -> {
                Event packet = super.instance.getGson().fromJson(message.content(), Event.class);
                new Thread(() -> {
                    HikariConnection hikariConnection = MatchPlugin.getInstance().getHikariCP();
                    hikariConnection.doQuery("INSERT INTO `events` (`game`, `player`, `action`, `date`) VALUES ('" + packet.getGameId() + "', '" + packet.getPlayer() + "', '" + packet.getAction().name() + "', '" + new Date(packet.getTimestamp()) + "');");
                }).start();
            }
            case REMOVE_SPECTATOR -> {
                RemoveSpectators packet = super.instance.getGson().fromJson(message.content(), RemoveSpectators.class);
                Arrays.stream(packet.getSpectators())
                        .map(MatchPlugin.getInstance().getServer()::getPlayer)
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .forEach(player -> Server.getLobbies().stream().findAny().ifPresent(lobby -> finishPlayers(player, lobby)));
            }
            case START_GAME -> {
                StartGame packet = super.instance.getGson().fromJson(message.content(), StartGame.class);
                List<List<MatchUser>> users = packet.getTeams().stream()
                        .map(uuids ->
                                Arrays.stream(uuids)
                                        .map(MatchUser::getUser)
                                        .filter(Optional::isPresent)
                                        .map(Optional::get).toList())
                        .toList();
                for (List<MatchUser> matchUserStream : users) {
                    String[] opponentsList = users.stream()
                            .filter(matchUsers -> matchUsers != matchUserStream)
                            .flatMap(Collection::stream)
                            .map(opponent -> MatchMessages.PLAYER.display().replace("{0}", opponent.getName()))
                            .toArray(String[]::new);
                    matchUserStream.forEach(user -> {
                        user.setRanked(packet.isRanked());
                        user.setPlaying(true);
                        user.getPlayer().ifPresent(player ->
                                Messages.sendMessage(player, MatchMessages.MATCH_FIND.display(), String.join("&7 ", opponentsList).replace("&", "§"))
                        );
                    });
                }
                if (packet.isRanked()) {
                    new Thread(() -> {
                        HikariConnection hikariConnection = MatchPlugin.getInstance().getHikariCP();
                        hikariConnection.doQuery("INSERT INTO `games` (`uuid`, `arena`, `server`, `date`) VALUES ('" + packet.getGameId() + "', '" + packet.getArenaName() + "', '" + message.source() + "', '" + new Date(packet.getTimestamp()) + "');");
                        users.forEach(team ->
                                team.forEach(user ->
                                        hikariConnection.doQuery("INSERT INTO `playing` (`game`, `player`, `status`) VALUES ('" + packet.getGameId() + "', '" + user.getUniqueId() + "', '0');")
                                )
                        );
                    }).start();
                }
            }
            case STATS_REQUEST -> {
                StatsRequest packet = super.instance.getGson().fromJson(message.content(), StatsRequest.class);
                MatchPlugin.getInstance().getHikariCP().readQuery(this.request + " WHERE `uuid` = '" + packet.getUser() + "' OR `name` = '" + packet.getUser() + "';", resultSet -> {
                    try {
                        if (resultSet.next()) {
                            int elo = resultSet.getInt("elo");
                            Stats stats = new Stats(
                                    UUID.fromString(resultSet.getString("uuid")),
                                    MatchPlugin.getInstance().getRank(elo),
                                    resultSet.getString("name"),
                                    elo,
                                    resultSet.getInt("kills"),
                                    resultSet.getInt("deaths"),
                                    resultSet.getInt("wins"),
                                    resultSet.getInt("games"),
                                    new int[]{
                                            resultSet.getInt("elo_rank"),
                                            resultSet.getInt("kills_rank"),
                                            resultSet.getInt("deaths_rank"),
                                            resultSet.getInt("wins_rank"),
                                            resultSet.getInt("games_rank")
                                    }
                            );
                            super.instance.sendMessage(new Message(message.source(), "proxy", Message.MessageType.STATS_RESPONSE, super.instance.getGson().toJson(new StatsResponse(packet.getUser(), stats))));
                            return;
                        }
                        super.instance.sendMessage(new Message(message.source(), "proxy", Message.MessageType.STATS_RESPONSE, super.instance.getGson().toJson(new StatsResponse(packet.getUser(), null))));
                    } catch (SQLException | IOException | TimeoutException ignored) {
                    }
                }, () -> {
                    try {
                        super.instance.sendMessage(new Message(message.source(), "proxy", Message.MessageType.STATS_RESPONSE, super.instance.getGson().toJson(new StatsResponse(packet.getUser(), null))));
                    } catch (IOException | TimeoutException e) {
                        throw new RuntimeException(e);
                    }
                });
            }
            case STOPPING_SERVER -> ClusterServer.removeServer(message.source());
            case TOP_PLAYERS_REQUEST -> {
                TopPlayersRequest packet = super.instance.getGson().fromJson(message.content(), TopPlayersRequest.class);
                String column = packet.getMode();
                MatchPlugin.getInstance().getHikariCP().readQuery(this.request + " ORDER BY `" + column.toLowerCase() + "_rank` LIMIT " + packet.getLimit() + ";", resultSet -> {
                    List<Stats> stats = new ArrayList<>();
                    try {
                        while (resultSet.next() && stats.size() < packet.getLimit()) {
                            try {
                                int elo = resultSet.getInt("elo");
                                stats.add(new Stats(
                                        UUID.fromString(resultSet.getString("uuid")),
                                        MatchPlugin.getInstance().getRank(elo),
                                        resultSet.getString("name"),
                                        elo,
                                        resultSet.getInt("kills"),
                                        resultSet.getInt("deaths"),
                                        resultSet.getInt("wins"),
                                        resultSet.getInt("games"),
                                        new int[]{
                                                resultSet.getInt("elo_rank"),
                                                resultSet.getInt("games_rank"),
                                                resultSet.getInt("wins_rank"),
                                                resultSet.getInt("kills_rank"),
                                                resultSet.getInt("deaths_rank")
                                        }
                                ));
                            } catch (IllegalArgumentException ignored) {
                            }
                        }
                        super.instance.sendMessage(new Message(message.source(), "proxy", Message.MessageType.TOP_PLAYERS_RESPONSE, super.instance.getGson().toJson(new TopPlayerResponse(packet.getMode(), stats))));
                    } catch (SQLException | IOException | TimeoutException e) {
                        e.printStackTrace();
                    }
                }, () -> {
                    try {
                        super.instance.sendMessage(new Message(message.source(), "proxy", Message.MessageType.TOP_PLAYERS_RESPONSE, super.instance.getGson().toJson(new TopPlayerResponse(packet.getMode(), new ArrayList<>()))));
                    } catch (IOException | TimeoutException ignored) {
                    }
                });
            }
            case HAS_BENDING -> {
                MatchPlugin.getInstance().completeBend(super.instance.getGson().fromJson(message.content(), HasBending.class).getPlayer());
            }
            default -> {
            }
        }
    }

    private void finishPlayers(List<MatchUser> players, Title title, LobbyServer lobby) {
        players.forEach(matchUser -> {
            matchUser.setPlaying(false);
            matchUser.setRanked(true);
            matchUser.getPlayer().ifPresent(player -> {
                player.createConnectionRequest(lobby.getServer()).fireAndForget();
                MatchPlugin.getInstance().getServer().getScheduler().buildTask(MatchPlugin.getInstance(), () ->
                        player.showTitle(title)
                ).delay(Duration.ofSeconds(3)).schedule();
            });
        });
    }

    private void finishPlayers(List<Player> players, LobbyServer lobby) {
        players.forEach(player -> player.createConnectionRequest(lobby.getServer()).fireAndForget());
    }

    private void finishPlayers(Player player, LobbyServer lobby) {
        player.createConnectionRequest(lobby.getServer()).fireAndForget();
    }

    private int eloToWin(int difference, int base, double curves) {
        return (int) Math.round(2 * (base) - (1d / (1d + Math.exp(-curves * difference)) * (2 * base - 2) + 1));
    }

}
