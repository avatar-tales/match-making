package fr.bakaaless.matchmaking.proxy.process;

import fr.bakaaless.matchmaking.common.games.ArenaSettings;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class WaitingGroup {

    private final List<WaitingUser> users;
    private final ArenaSettings     settings;
    private       int               averageElo;

    public WaitingGroup(final ArenaSettings settings) {
        this.users = new ArrayList<>();
        this.settings = settings;
        this.averageElo = 0;
    }

    public List<WaitingUser> getUsers() {
        return this.users;
    }

    public void addUser(final WaitingUser user) {
        this.users.add(user);
        this.updateAverageElo();
    }

    public void removeUser(final WaitingUser user) {
        this.users.remove(user);
        this.updateAverageElo();
    }

    public void removeUser(UUID uuid) {
        this.users.removeIf(user -> user.getUser().getUniqueId().equals(uuid));
        this.updateAverageElo();
    }

    public void updateAverageElo() {
        if (this.users.isEmpty()) {
            this.averageElo = 0;
            return;
        }
        int sum = 0;
        for (final WaitingUser user : this.users) {
            sum += user.getUser().getElo();
        }
        this.averageElo = sum / this.users.size();
    }

    public int distance(final int elo1, final int elo2, final int offset) {
        final int dist = Math.abs(elo1 - elo2);
        return dist > offset ? Integer.MAX_VALUE : dist;
    }

    public WaitingUser nearestUser(final WaitingUser user) {
        WaitingUser nearestUser = null;
        int distance = Integer.MAX_VALUE;
        int localDistance;
        for (final WaitingUser users : this.users) {
            if (users == user) continue;
            localDistance = this.distance(users.getUser().getElo(), user.getUser().getElo(), user.getEloOffset());
            if (localDistance < distance) {
                nearestUser = users;
                distance = localDistance;
            }
        }
        return nearestUser;
    }

    public int size() {
        return this.users.size();
    }

    public boolean isEmpty() {
        return this.users.isEmpty();
    }

    public ArenaSettings getArenaSettings() {
        return this.settings;
    }

    public int getAverageElo() {
        return this.averageElo;
    }

    @Override
    public String toString() {
        return "WaitingGroup{" + "users=" + users + ", settings=" + settings + ", averageElo=" + averageElo + '}';
    }
}
