package fr.bakaaless.matchmaking.proxy.process;

import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ServerConnection;
import fr.bakaaless.matchmaking.common.games.ArenaSettings;
import fr.bakaaless.matchmaking.common.utils.Pair;
import fr.bakaaless.matchmaking.proxy.MatchPlugin;
import fr.bakaaless.matchmaking.proxy.data.ClusterServer;
import fr.bakaaless.matchmaking.proxy.data.MatchUser;
import fr.bakaaless.matchmaking.proxy.data.Server;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeoutException;

public class MatchMaking implements Runnable {

    private final PriorityQueue<WaitingUser> users;

    private final LinkedList<Pair<WaitingUser[], Integer>> duels;
    private final Map<ArenaSettings, List<WaitingGroup>> groups;

    private final long thresholdLoop;

    public MatchMaking() {
        this.users = new PriorityQueue<>();
        this.duels = new LinkedList<>();
        this.groups = new HashMap<>();
        this.thresholdLoop = MatchPlugin.getInstance().getConfig().get("match_making.threshold", 10000);
    }

    @Override
    public void run() {
        for (WaitingUser waitingUser : this.users) {
            waitingUser.getUser().getPlayer().ifPresent(player ->
                    player.sendActionBar(LegacyComponentSerializer.legacyAmpersand().deserialize("&7" + waitingUser.parseTime() + " " + "&8&o(" + waitingUser.getEloOffset() + ")"))
            );
        }
        for (Pair<WaitingUser[], Integer> waitingUsers : this.duels) {
            Arrays.stream(waitingUsers.getKey())
                    .forEach(waitingUser ->
                            waitingUser.getUser().getPlayer().ifPresent(player ->
                                    player.sendActionBar(LegacyComponentSerializer.legacyAmpersand().deserialize("&7" + waitingUser.parseTime()))
                            )
                    );
        }
        if (this.duels.size() > 0 && Math.random() > 0.5) {
            Pair<WaitingUser[], Integer> duel = this.duels.poll();

            final List<ClusterServer> potentialServers = Server.getFilteredClusterServers(server ->
                    server.getArenas().stream().anyMatch(settings -> settings.matchType(ArenaSettings.wildCard()))
            );
            final Optional<ClusterServer> potentialServer = potentialServers.stream()
                    .filter(server -> server.getArenas().size() > 0)
                    .findAny();
            potentialServer.ifPresentOrElse(clusterServer -> MatchMaking.sendPlayers(clusterServer, false, duel.getValue(), duel.getKey()), () -> this.duels.offerFirst(duel));
        }
        if (this.users.isEmpty())
            return;
        final WaitingUser user = this.users.poll();
        if (user == null)
            return;
        final WaitingGroup group = user.getGroup();

        final WaitingGroup waitingGroup = new WaitingGroup(user.getArenaSettings());
        waitingGroup.addUser(user);
        group.removeUser(user);
        for (int i = 1; i < user.getArenaSettings().getTeamsNumber() * user.getArenaSettings().getTeamSize(); i++) {
            if (group.isEmpty()) {
                WaitingGroup nearestGroup = null;
                int distance = Integer.MAX_VALUE;
                int localDistance;
                for (final WaitingGroup groups : this.groupsMatching(user.getArenaSettings())) {
                    if (groups == group || groups.isEmpty())
                        continue;
                    localDistance = groups.distance(groups.getAverageElo(), waitingGroup.getAverageElo(), user.getEloOffset());
                    if (localDistance < distance) {
                        nearestGroup = groups;
                        distance = localDistance;
                    }
                }
                if (nearestGroup == null)
                    continue;

                final WaitingUser nearestUser = nearestGroup.nearestUser(user);
                if (nearestUser != null) {
                    waitingGroup.addUser(nearestUser);
                    group.removeUser(nearestUser);
                }

            } else {
                final WaitingUser nearestUser = group.nearestUser(user);
                if (nearestUser != null) {
                    waitingGroup.addUser(nearestUser);
                    group.removeUser(nearestUser);
                }
            }
        }

        if (waitingGroup.size() == user.getArenaSettings().getTeamSize() * user.getArenaSettings().getTeamsNumber()) {
            final List<ClusterServer> potentialServers = Server.getFilteredClusterServers(server ->
                    server.getArenas().stream().anyMatch(settings -> settings.matchType(user.getArenaSettings()))
            );
            final Optional<ClusterServer> potentialServer = potentialServers.stream()
                    .filter(server -> server.getArenas().size() > 0)
                    .findAny();

            if (potentialServer.isPresent()) {
                waitingGroup.getUsers().forEach(players -> this.removePlayer(players.getUser()));
                sendPlayers(potentialServer.get(), true, 0, waitingGroup.getUsers().toArray(new WaitingUser[0]));
                return;
            }
        }
        user.updateTime(this.thresholdLoop);
        this.users.offer(user);
        for (final WaitingUser matchingUser : waitingGroup.getUsers())
            matchingUser.getGroup().addUser(matchingUser);
    }

    public void addUser(final WaitingUser user) {
        if (this.users.stream().anyMatch(users -> users.getUser().getUniqueId().equals(user.getUser().getUniqueId())))
            return;
        WaitingGroup nearestGroup = new WaitingGroup(user.getArenaSettings());
        if (!this.groups.containsKey(user.getArenaSettings())) {
            this.groups.put(user.getArenaSettings(), new ArrayList<>());
            this.groups.get(user.getArenaSettings()).add(nearestGroup);
        } else {
            int distance = nearestGroup.distance(nearestGroup.getAverageElo(), user.getUser().getElo(), user.getEloOffset());
            int localDistance;
            for (final WaitingGroup group : this.groupsMatching(user.getArenaSettings())) {
                localDistance = group.distance(group.getAverageElo(), user.getUser().getElo(), user.getEloOffset());
                if (localDistance < distance) {
                    nearestGroup = group;
                    distance = localDistance;
                }
            }
            if (!this.groups.get(user.getArenaSettings()).contains(nearestGroup))
                this.groups.get(user.getArenaSettings()).add(nearestGroup);
        }
        user.setGroup(nearestGroup);
        nearestGroup.addUser(user);
        this.users.offer(user);
    }

    public void removePlayer(MatchUser user) {
        this.users.removeIf(waitingUser -> {
            if (waitingUser.getUser().getUniqueId().equals(user.getUniqueId())) {
                waitingUser.getGroup().removeUser(waitingUser);
                waitingUser.setGroup(null);
                return true;
            }
            return false;
        });
        this.groups.values().removeIf(waitingGroups -> {
            waitingGroups.removeIf(waitingGroup -> {
                waitingGroup.removeUser(user.getUniqueId());
                return waitingGroup.isEmpty();
            });
            return waitingGroups.isEmpty();
        });
    }

    public static void sendPlayers(ClusterServer server, boolean ranked, int round, @NotNull WaitingUser... users) {

        final Map<Player, ServerConnection> initialServer = new HashMap<>();
        try {
            for (final WaitingUser user : users) {
                MatchPlugin.getInstance().getServer().getPlayer(user.getUser().getUniqueId()).ifPresent(
                        player -> {
                            if (Objects.equals(server.getServer(), player.getCurrentServer().get().getServer()))
                                return;
                            initialServer.put(player, player.getCurrentServer().get());
                            player.createConnectionRequest(server.getServer()).fireAndForget();
                        }
                );
            }
            MatchPlugin.getInstance().getServer().getScheduler().buildTask(MatchPlugin.getInstance(), () ->
                    {
                        try {
                            MatchPlugin.getInstance().getRabbitMQ().sendGroup(users, server, users[0].getArenaSettings(), ranked, round);
                        } catch (IOException | TimeoutException e) {
                            throw new RuntimeException(e);
                        }
                    }
            ).delay(Duration.ofMillis(1000)).schedule();
        } catch (Exception e) {
            e.printStackTrace();
            initialServer.forEach((player, connection) -> player.createConnectionRequest(connection.getServer()).fireAndForget());
        }
    }

    private List<WaitingGroup> groupsMatching(ArenaSettings settings) {
        return this.groups.keySet().stream().filter(settings::matchType).map(this.groups::get).flatMap(Collection::stream).toList();
    }

    public void addDuel(WaitingUser[] users, Integer points) {
        this.duels.add(new Pair<>(users, points));
        this.removePlayer(users[0].getUser());
        this.removePlayer(users[1].getUser());
    }

    public int size() {
        return this.users.size();
    }

}
