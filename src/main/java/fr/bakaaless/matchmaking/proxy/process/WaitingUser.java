package fr.bakaaless.matchmaking.proxy.process;

import fr.bakaaless.matchmaking.common.games.ArenaSettings;
import fr.bakaaless.matchmaking.proxy.MatchPlugin;
import fr.bakaaless.matchmaking.proxy.data.MatchUser;
import org.jetbrains.annotations.NotNull;

public class WaitingUser implements Comparable<WaitingUser> {

    private final MatchUser     user;
    private final ArenaSettings settings;
    private final long          realTime;
    private       long          time;
    private       WaitingGroup  group;

    public WaitingUser(final MatchUser user, final ArenaSettings settings) {
        this.user = user;
        this.settings = settings;
        this.realTime = System.currentTimeMillis();
        this.time = System.currentTimeMillis();
    }

    public MatchUser getUser() {
        return this.user;
    }

    public ArenaSettings getArenaSettings() {
        return this.settings;
    }

    public long getTime() {
        return this.time;
    }

    public void updateTime(final long added) {
        this.time += added;
    }

    public WaitingGroup getGroup() {
        return this.group;
    }

    public void setGroup(WaitingGroup group) {
        this.group = group;
    }

    public int getEloOffset() {
        return (int) (
                MatchPlugin.getInstance().getConfig().get("match_making.elo_range.base", 20) +
                        MatchPlugin.getInstance().getConfig().get("match_making.elo_range.multiplier", 15) *
                                (System.currentTimeMillis() - this.realTime) /
                                MatchPlugin.getInstance().getConfig().get("match_making.elo_range.time", 60 * 1000)
        );
    }

    public String parseTime() {
        int millis = (int) (System.currentTimeMillis() - this.realTime);
        int seconds = (millis / 1000) % 60;
        int minutes = (millis / 1000) / 60;
        StringBuilder stringBuilder = new StringBuilder();
        if (minutes < 10)
            stringBuilder.append("0");
        stringBuilder.append(minutes).append(":");
        if (seconds < 10)
            stringBuilder.append("0");
        stringBuilder.append(seconds);
        return stringBuilder.toString();
    }

    @Override
    public int compareTo(final @NotNull WaitingUser other) {
        return Long.compare(this.getTime(), other.getTime());
    }

    @Override
    public String toString() {
        return "WaitingUser{" +
                "user=" + user +
                ", settings=" + settings +
                ", time=" + time +
                ", group=" + group.getUsers().stream().map(user -> user.getUser().getName()).toList() +
                '}';
    }
}
