package fr.bakaaless.matchmaking.proxy.commands;

import com.velocitypowered.api.command.CommandMeta;
import com.velocitypowered.api.proxy.Player;
import fr.bakaaless.matchmaking.common.games.ArenaSettings;
import fr.bakaaless.matchmaking.common.messaging.Message;
import fr.bakaaless.matchmaking.common.messaging.messages.game.AddSpectators;
import fr.bakaaless.matchmaking.common.utils.Pair;
import fr.bakaaless.matchmaking.proxy.MatchPlugin;
import fr.bakaaless.matchmaking.proxy.config.MatchMessages;
import fr.bakaaless.matchmaking.proxy.data.MatchUser;
import fr.bakaaless.matchmaking.proxy.process.WaitingUser;
import fr.bakaaless.proxycore.commands.ProxyCommand;
import fr.bakaaless.proxycore.config.Messages;
import fr.bakaaless.proxycore.config.Parameter;

import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.function.IntSupplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DuelCommand extends ProxyCommand {

    @Parameter
    private final String usage = "/duel <accept | refuse | player>";

    private final Map<Player, Pair<Player, Integer>> invitations;

    public DuelCommand(String command, String[] aliases) {
        super(command);
        this.setPermission("matchmaking.commands");
        this.invitations = new HashMap<>();
        CommandMeta meta = MatchPlugin.getInstance().getServer().getCommandManager().metaBuilder(command)
                .aliases(aliases)
                .build();

        MatchPlugin.getInstance().getServer().getCommandManager().register(meta, this);
    }

    @Override
    public void execute(Invocation invocation) {
        if (invocation.arguments().length == 0) {
            Messages.ERROR_USAGE.send(invocation.source(), this.usage);
            return;
        }
        if (invocation.source() instanceof Player proxyPlayer) {
            MatchUser.getUser(proxyPlayer.getUniqueId()).ifPresent(player -> {
                if (player.isPlaying()) {
                    Messages.sendMessage(proxyPlayer, MatchMessages.ALREADY_PLAYING_SELF.display());
                    return;
                }
                if (invocation.arguments()[0].equalsIgnoreCase("accept")) {
                    if (!this.invitations.containsKey(proxyPlayer)) {
                        Messages.sendMessage(proxyPlayer, MatchMessages.NO_ONLINE.display());
                        return;
                    }
                    Pair<Player, Integer> challengerData = this.invitations.remove(proxyPlayer);
                    Player challenger = challengerData.getKey();
                    if (!challenger.isActive()) {
                        Messages.sendMessage(proxyPlayer, MatchMessages.NO_ONLINE.display());
                        return;
                    }
                    MatchUser.getUser(challenger.getUniqueId()).ifPresent(challengerUser -> {
                        if (challengerUser.isPlaying()) {
                            Messages.sendMessage(proxyPlayer, MatchMessages.ALREADY_PLAYING_OTHER.display());
                            return;
                        }
                        MatchUser.getUser(proxyPlayer.getUniqueId()).ifPresent(playerUser -> {
                            if (playerUser.isPlaying()) {
                                Messages.sendMessage(proxyPlayer, MatchMessages.ALREADY_PLAYING_SELF.display());
                                return;
                            }
                            Messages.sendMessage(challenger, MatchMessages.INVITATION_ACCEPT.display(), proxyPlayer.getUsername());
                            MatchPlugin.getInstance().getMatchMaking().addDuel(new WaitingUser[]{new WaitingUser(playerUser, ArenaSettings.wildCard()), new WaitingUser(challengerUser, ArenaSettings.wildCard())}, challengerData.getValue());
                        });
                    });
                    return;
                } else if (invocation.arguments()[0].equalsIgnoreCase("refuse")) {
                    if (!this.invitations.containsKey(proxyPlayer)) {
                        return;
                    }
                    Player challenger = this.invitations.remove(proxyPlayer).getKey();
                    if (!challenger.isActive()) {
                        Messages.sendMessage(proxyPlayer, MatchMessages.NO_ONLINE.display());
                        return;
                    }
                    Messages.sendMessage(challenger, MatchMessages.INVITATION_REFUSE_OTHER.display(), proxyPlayer.getUsername());
                    Messages.sendMessage(proxyPlayer, MatchMessages.INVITATION_REFUSE_SELF.display(), challenger.getUsername());
                    return;
                } else if (invocation.arguments()[0].equalsIgnoreCase("spectate")) {
                    MatchPlugin.getInstance().getServer().getPlayer(invocation.arguments()[1]).ifPresentOrElse(viewer ->
                                    MatchUser.getUser(viewer.getUniqueId()).ifPresent(viewerUser -> {
                                        if (!viewerUser.isPlaying()) {
                                            Messages.sendMessage(proxyPlayer, MatchMessages.NO_PLAYING.display());
                                            return;
                                        }
                                        if (viewerUser.isRanked()) {
                                            Messages.sendMessage(proxyPlayer, MatchMessages.RANKED_MOD.display());
                                            return;
                                        }
                                        MatchPlugin.getInstance().getMatchMaking().removePlayer(player);
                                        if (proxyPlayer.getCurrentServer().isPresent())
                                            viewer.getCurrentServer().ifPresent(serverConnection -> {
                                                if (!serverConnection.getServerInfo().getName().equalsIgnoreCase(proxyPlayer.getCurrentServer().get().getServerInfo().getName()))
                                                    proxyPlayer.createConnectionRequest(serverConnection.getServer()).fireAndForget();
                                            });

                                        MatchPlugin.getInstance().getServer().getScheduler().buildTask(MatchPlugin.getInstance(), () -> {
                                            try {
                                                MatchPlugin.getInstance().getRabbitMQ().sendMessage(viewerUser.getConnection().getName(), "proxy", Message.MessageType.ADD_SPECTATOR, new AddSpectators(viewerUser.getUniqueId(), new UUID[]{proxyPlayer.getUniqueId()}));
                                            } catch (IOException | TimeoutException e) {
                                                e.printStackTrace();
                                            }
                                        }).delay(Duration.ofMillis(1000)).schedule();
                                    })
                            , () -> Messages.sendMessage(proxyPlayer, MatchMessages.NO_FOUND.display()));
                }
                MatchPlugin.getInstance().getServer().getPlayer(invocation.arguments()[0]).ifPresentOrElse(opponent -> {
                    if (this.invitations.containsKey(proxyPlayer) && this.invitations.get(proxyPlayer).getKey().getUniqueId().equals(opponent.getUniqueId())) {
                        MatchPlugin.getInstance().getServer().getCommandManager().executeAsync(proxyPlayer, "duel accept");
                        return;
                    }
                    MatchUser.getUser(proxyPlayer.getUniqueId()).ifPresent(opponentPlayer -> {
                        if (opponentPlayer.isPlaying()) {
                            Messages.sendMessage(proxyPlayer, MatchMessages.ALREADY_PLAYING_OTHER.display());
                            return;
                        }
                        if (opponent.getUniqueId().equals(proxyPlayer.getUniqueId())) {
                            Messages.sendMessage(proxyPlayer, MatchMessages.NO_SELF.display());
                            return;
                        }
                        int points = 3;
                        try {
                            if (invocation.arguments().length == 2)
                                points = Integer.parseInt(invocation.arguments()[1]);
                        } catch (Exception ignored) {}
                        points = Math.min(MatchPlugin.getInstance().getConfig().get("duels.rounds.max", 5), Math.max(points, MatchPlugin.getInstance().getConfig().get("duels.rounds.min", 1)));
                        Messages.sendMessage(proxyPlayer, MatchMessages.INVITATION_SEND.display(), opponent.getUsername(), String.valueOf(points));
                        Messages.sendMessage(opponent, MatchMessages.INVITATION_RECEIVE.display(), proxyPlayer.getUsername(), String.valueOf(points));
                        this.invitations.put(opponent, new Pair<>(proxyPlayer, points));
                    });
                }, () -> Messages.sendMessage(proxyPlayer, MatchMessages.NO_FOUND.display()));

            });
        } else {
            Messages.ERROR_EXECUTOR_PLAYER.send(invocation.source());
        }
    }

    @Override
    public List<String> suggest(Invocation invocation) {
        if (invocation.arguments().length < 2)
            return Stream.of(
                            MatchPlugin.getInstance()
                                    .getServer()
                                    .getAllPlayers()
                                    .stream().map(Player::getUsername)
                                    .toList(),
                            List.of("accept", "refuse", "spectate")
                    )
                    .flatMap(List::stream)
                    .filter(entry -> invocation.arguments().length == 0 || entry.toLowerCase().startsWith(invocation.arguments()[0].toLowerCase()))
                    .sorted()
                    .toList();
        else if (invocation.arguments()[0].equalsIgnoreCase("spectate"))
            return MatchPlugin.getInstance().getServer().getAllPlayers()
                    .stream()
                    .map(Player::getUsername)
                    .filter(entry -> entry.toLowerCase().startsWith(invocation.arguments()[1].toLowerCase()))
                    .sorted()
                    .toList();
        else if (!invocation.arguments()[0].equalsIgnoreCase("accept") && !invocation.arguments()[0].equalsIgnoreCase("refuse")){
            return IntStream.rangeClosed(1, 10)
                    .mapToObj(String::valueOf)
                    .filter(entry -> entry.toLowerCase().startsWith(invocation.arguments()[1].toLowerCase()))
                    .sorted()
                    .toList();
        }
        return super.suggest(invocation);
    }
}
