package fr.bakaaless.matchmaking.proxy.commands;

import com.velocitypowered.api.command.CommandMeta;
import com.velocitypowered.api.proxy.Player;
import fr.bakaaless.matchmaking.common.games.ArenaSettings;
import fr.bakaaless.matchmaking.common.messaging.Message;
import fr.bakaaless.matchmaking.common.messaging.messages.game.AddSpectators;
import fr.bakaaless.matchmaking.proxy.MatchPlugin;
import fr.bakaaless.matchmaking.proxy.config.MatchMessages;
import fr.bakaaless.matchmaking.proxy.data.MatchUser;
import fr.bakaaless.matchmaking.proxy.data.Server;
import fr.bakaaless.matchmaking.proxy.process.WaitingUser;
import fr.bakaaless.proxycore.commands.ProxyCommand;
import fr.bakaaless.proxycore.config.Messages;
import fr.bakaaless.proxycore.config.Parameter;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.stream.Stream;

public class MatchMakingCommand extends ProxyCommand {

    @Parameter
    private final String usage = "/matchmaking <join | leave | spectate>";

    public MatchMakingCommand(String command, String[] aliases) {
        super(command);
        this.setPermission("matchmaking.commands");
        CommandMeta meta = MatchPlugin.getInstance().getServer().getCommandManager().metaBuilder(command)
                .aliases(aliases)
                .build();

        MatchPlugin.getInstance().getServer().getCommandManager().register(meta, this);
    }

    @Override
    public void execute(Invocation invocation) {
        if (invocation.arguments().length == 0) {
            Messages.ERROR_USAGE.send(invocation.source(), this.usage);
            return;
        }
        if (invocation.source() instanceof Player proxyPlayer) {
            MatchUser.getUser(proxyPlayer.getUniqueId()).ifPresent(player -> {
                if (player.isPlaying()) {
                    Messages.sendMessage(proxyPlayer, MatchMessages.ALREADY_PLAYING_SELF.display());
                    return;
                }
                if (invocation.arguments()[0].equalsIgnoreCase("join")) {
                    if (Server.getFilteredClusterServers(server -> true).isEmpty()) {
                        Messages.sendMessage(proxyPlayer, MatchMessages.NO_SERVER.display());
                        return;
                    }
                    try {
                        if (!MatchPlugin.getInstance().hasBend(player.getUniqueId())) {
                            Messages.sendMessage(proxyPlayer, MatchMessages.NO_BEND.display());
                            return;
                        }
                    } catch (IOException | TimeoutException | ExecutionException | InterruptedException ignored) {
                    }
                    MatchPlugin.getInstance().getMatchMaking().addUser(new WaitingUser(player, ArenaSettings.wildCard()));
                    Messages.sendMessage(proxyPlayer, MatchMessages.QUEUE_JOIN.display(), player.getName(), String.valueOf(MatchPlugin.getInstance().getMatchMaking().size()));
                } else if (invocation.arguments()[0].equalsIgnoreCase("leave")) {
                    MatchPlugin.getInstance().getMatchMaking().removePlayer(player);
                    Messages.sendMessage(proxyPlayer, MatchMessages.QUEUE_LEAVE.display());
                }
            });
        } else {
            Messages.ERROR_EXECUTOR_PLAYER.send(invocation.source());
        }
    }

    @Override
    public List<String> suggest(Invocation invocation) {
        if (invocation.arguments().length < 2)
            return Stream.of(
                            List.of("join", "leave")
                    )
                    .flatMap(List::stream)
                    .filter(entry -> invocation.arguments().length == 0 || entry.toLowerCase().startsWith(invocation.arguments()[0].toLowerCase()))
                    .sorted()
                    .toList();
        return super.suggest(invocation);
    }
}
