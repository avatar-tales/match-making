package fr.bakaaless.matchmaking.proxy.listeners;

import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.DisconnectEvent;
import com.velocitypowered.api.event.connection.LoginEvent;
import com.velocitypowered.api.event.player.ServerConnectedEvent;
import fr.bakaaless.matchmaking.common.databases.HikariConnection;
import fr.bakaaless.matchmaking.proxy.MatchPlugin;
import fr.bakaaless.matchmaking.proxy.data.MatchUser;
import fr.bakaaless.matchmaking.proxy.data.Server;

import java.sql.SQLException;

public class Connection {

    @Subscribe
    public void onPlayerJoin(LoginEvent event) {
        HikariConnection hikariConnection = MatchPlugin.getInstance().getHikariCP();
        int baseElo = MatchPlugin.getInstance().getConfig().getOrWrite("match_making.elo.default", 100);
        hikariConnection.doQuery("INSERT INTO `players` (`uuid`, `name`, `elo`) VALUES ('" + event.getPlayer().getUniqueId() + "', '" + event.getPlayer().getUsername() + "', '" + baseElo + "') ON CONFLICT DO UPDATE SET name = '" + event.getPlayer().getUsername() + "';");
        hikariConnection.readQuery("SELECT `elo` FROM `players` WHERE `uuid` = '" + event.getPlayer().getUniqueId() + "';", resultSet -> {
            try {
                if (resultSet.next()) {
                    int elo = resultSet.getInt("elo");
                    MatchUser.register(event.getPlayer(), elo);
                    return;
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            MatchUser.register(event.getPlayer(), baseElo);
        }, () -> MatchUser.register(event.getPlayer(), baseElo));
    }

    @Subscribe
    public void onPlayerJoinServer(ServerConnectedEvent event) {
        MatchUser.getUser(event.getPlayer().getUniqueId()).ifPresent(player -> {
            if (player.getConnection() != null)
                player.getConnection().getConnectedPlayers().remove(player);
            Server.getServersByServer(event.getServer()).stream().findAny().ifPresent(server -> {
                player.setConnection(server);
                server.getConnectedPlayers().add(player);
            });
        });
    }

    @Subscribe
    public void onPlayerLeaveProxy(DisconnectEvent event) {
        MatchUser.getUser(event.getPlayer().getUniqueId()).ifPresent(player -> {
            if (player.getConnection() != null)
                player.getConnection().getConnectedPlayers().remove(player);
            MatchPlugin.getInstance().getMatchMaking().removePlayer(player);
            MatchUser.remove(player);
        });
    }
}
