package fr.bakaaless.matchmaking.proxy;

import com.google.inject.Inject;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.event.proxy.ProxyShutdownEvent;
import com.velocitypowered.api.plugin.Dependency;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.plugin.annotation.DataDirectory;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.scheduler.ScheduledTask;
import fr.bakaaless.matchmaking.common.configuration.Config;
import fr.bakaaless.matchmaking.common.databases.HikariConnection;
import fr.bakaaless.matchmaking.common.games.Rank;
import fr.bakaaless.matchmaking.common.messaging.Message;
import fr.bakaaless.matchmaking.common.utils.Pair;
import fr.bakaaless.matchmaking.proxy.commands.DuelCommand;
import fr.bakaaless.matchmaking.proxy.commands.MatchMakingCommand;
import fr.bakaaless.matchmaking.proxy.config.MatchMessages;
import fr.bakaaless.matchmaking.proxy.data.MatchUser;
import fr.bakaaless.matchmaking.proxy.listeners.Connection;
import fr.bakaaless.matchmaking.proxy.messaging.RabbitMQProxy;
import fr.bakaaless.matchmaking.proxy.messaging.RabbitMQProxyConsumer;
import fr.bakaaless.matchmaking.proxy.process.MatchMaking;
import fr.bakaaless.matchmaking.proxy.updater.KeepAlive;
import fr.bakaaless.proxycore.commands.ProxyCommand;
import fr.bakaaless.proxycore.config.Messages;
import fr.bakaaless.proxycore.config.Parameter;
import org.slf4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Plugin(
        id = "matchmaking",
        name = "Matchmaking",
        version = "@VERSION@",
        authors = "BakaAless",
        dependencies = {
                @Dependency(
                        id = "proxycore"
                )
        }
)
public class MatchPlugin {

    private static MatchPlugin instance;
    private final  ProxyServer server;
    private final  Logger      logger;
    private final  Path        dataDirectory;
    private final  List<Rank>  ranks;
    private        Config      config;

    private RabbitMQProxy    rabbitMQ;
    private Thread           rabbitConsumer;
    private HikariConnection hikariConnection;

    private MatchMaking   matchMaking;
    private ScheduledTask matchmakingScheduler;

    private Map<UUID, CompletableFuture<Boolean>> waitingForResponse;

    @Inject
    public MatchPlugin(final ProxyServer server, final Logger logger, final @DataDirectory Path dataDirectory) {
        instance = this;
        this.server = server;
        this.logger = logger;
        this.dataDirectory = dataDirectory;

        this.ranks = new ArrayList<>();
        this.waitingForResponse = new HashMap<>();
    }

    public static MatchPlugin getInstance() {
        return instance;
    }

    @Subscribe
    public void onProxyInitialize(final ProxyInitializeEvent event) {
        this.config = Config.load(this.dataDirectory, "config.yml", "/proxy/config.yml");
        this.server.getScheduler()
                .buildTask(this, () -> {
                    try {
                        this.downloadDrivers();
                        this.config.load();
                        MatchMessages.init(this.dataDirectory.toFile().getAbsolutePath(), "messages.yml");
                        try {
                            this.rabbitMQ = new RabbitMQProxy(this.dataDirectory);
                            this.rabbitConsumer = this.rabbitMQ.retrieveMessages(new RabbitMQProxyConsumer(this.rabbitMQ));
                            this.rabbitConsumer.setName("RabbitMQConsumer");
                            this.rabbitConsumer.setDaemon(true);
                            this.rabbitConsumer.start();
                            this.rabbitMQ.sendMessage(new Message("*", "proxy", Message.MessageType.PING, ""));

                            this.server.getScheduler()
                                    .buildTask(this, new KeepAlive())
                                    .repeat(this.config.get("servers.loop_ping", 10000), TimeUnit.MILLISECONDS)
                                    .schedule();
                        } catch (Exception e) {
                            this.logger.error("Can't load RabbitMQ", e);
                        }

                        try {
                            this.hikariConnection = new HikariConnection(this.dataDirectory);
                        } catch (Exception e) {
                            this.logger.error("Can't load HikariCP", e);
                        }

                        this.matchMaking = new MatchMaking();
                        this.matchmakingScheduler = server.getScheduler()
                                .buildTask(this, this.matchMaking)
                                .repeat(this.config.get("match_making.repeat", 1000), TimeUnit.MILLISECONDS)
                                .schedule();

                        this.server.getEventManager().register(this, new Connection());

                        this.setupCommand(new MatchMakingCommand(
                                        this.config.getOrWrite("commands.matchmaking.command", "matchmaking"),
                                        this.config.getOrWrite("commands.matchmaking.aliases", new ArrayList<String>()).toArray(new String[0])
                                ));
                        this.setupCommand(
                                new DuelCommand(
                                        this.config.getOrWrite("commands.duel.command", "duel"),
                                        this.config.getOrWrite("commands.duel.aliases", new ArrayList<String>()).toArray(new String[0])
                                ));

                        this.config.save();
                        this.setupRanks();
                        this.hookMessages();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                })
                .schedule();
    }

    @Subscribe
    public void onProxyShutdown(final ProxyShutdownEvent event) {
        if (this.matchmakingScheduler != null)
            this.matchmakingScheduler.cancel();
        if (this.rabbitConsumer != null)
            this.rabbitConsumer.interrupt();
    }

    private void downloadDrivers() {
        final Map<String, Pair<String, String>> toDownload = new HashMap<>();
        toDownload.put("amqp-client", new Pair<>("5.14.2", "https://repo1.maven.org/maven2/com/rabbitmq/amqp-client/{0}/amqp-client-{0}.jar"));
        toDownload.put("HikariCP", new Pair<>("5.0.1", "https://repo1.maven.org/maven2/com/zaxxer/HikariCP/{0}/HikariCP-{0}.jar"));
        toDownload.put("mariadb-driver", new Pair<>("3.0.4", "https://repo1.maven.org/maven2/org/mariadb/jdbc/mariadb-java-client/{0}/mariadb-java-client-{0}.jar"));
        toDownload.put("sqlite-driver", new Pair<>("3.36.0.3", "https://repo1.maven.org/maven2/org/xerial/sqlite-jdbc/{0}/sqlite-jdbc-{0}.jar"));
        toDownload.put("postgresql-driver", new Pair<>("42.3.4", "https://repo1.maven.org/maven2/org/postgresql/postgresql/{0}/postgresql-{0}.jar"));
        for (final String libName : toDownload.keySet()) {
            final Pair<String, String> info = toDownload.get(libName);
            try {
                final File libFile = new File(this.dataDirectory.toFile(), "librairies/" + libName + "-" + info.getKey() + ".jar");
                if (!libFile.exists()) {
                    this.logger.debug("Downloading " + libName + " " + info.getKey() + " ...");
                    libFile.getParentFile().mkdirs();
                    final String url = info.getValue().replace("{0}", info.getKey());
                    final URL download = new URL(url);

                    try (final ReadableByteChannel readableByteChannel = Channels.newChannel(download.openStream())) {
                        try (final FileOutputStream fileOutputStream = new FileOutputStream(libFile)) {
                            try (FileChannel fileChannel = fileOutputStream.getChannel()) {
                                fileChannel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
                            }
                        }
                    } catch (Exception e) {
                        this.logger.error("Can't download " + libName + " " + info.getLeft() + " from " + url, e);
                        return;
                    }
                    this.logger.debug(libName + " " + info.getLeft() + " downloaded.");
                }
                this.logger.debug("Importing " + libName + " " + info.getLeft() + " ...");
                this.server.getPluginManager().addToClasspath(this, Path.of(libFile.toURI()));
                this.logger.info(libName + " " + info.getLeft() + " imported !");
            } catch (IOException e) {
                this.logger.error(libName + " " + info.getLeft() + " importation aborted!", e);
            }
        }
    }

    private List<Field> getFields(Class<?> clazz) {
        final List<Field> fields = new ArrayList<>();
        while (clazz != Object.class) {
            fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
            clazz = clazz.getSuperclass();
        }
        return fields;
    }

    public void setupCommand(final ProxyCommand command) {
        this.getFields(command.getClass()).stream().filter(field -> field.isAnnotationPresent(Parameter.class))
                .forEach(field -> {
                    field.setAccessible(true);
                    try {
                        final StringBuilder path = new StringBuilder("commands.").append(command.getClass().getSimpleName().toLowerCase().replace("command", ""));
                        field.set(command, this.config.getOrWrite(path.append(".").append(field.getName()).toString(), field.get(command)));
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    private void setupRanks() {
        this.ranks.clear();
        for (String key : this.config.getKeys("ranks"))
            this.ranks.add(new Rank(this.config.get("ranks." + key + ".display", ""), this.config.get("ranks." + key + ".elo", 0)));
    }

    public String getRank(int elo) {
        if (this.ranks.isEmpty())
            return "";
        return this.ranks.stream()
                .filter(rank -> elo >= rank.elo())
                .max(Comparator.comparing(Rank::elo))
                .orElseGet(() -> this.ranks.get(0))
                .display();
    }

    public void hookMessages() {
        Pattern eloPattern = Pattern.compile("\\{mm_elo_(\\w{2,16})}", Pattern.CASE_INSENSITIVE);
        final Function<String, String> eloFunction = (message) -> {
            final Matcher matcher = eloPattern.matcher(message);
            final String username = matcher.results().toList().get(0).group(1);

            Optional<MatchUser> user = MatchUser.getUser(username);
            return user.map(matchUser -> String.valueOf(matchUser.getElo())).orElse("0");
        };
        Messages.registerHook(eloPattern, eloFunction);

        Pattern rankPattern = Pattern.compile("\\{mm_rank_(\\w{2,16})}", Pattern.CASE_INSENSITIVE);
        final Function<String, String> rankFunction = (message) -> {
            final Matcher matcher = rankPattern.matcher(message);
            final String username = matcher.results().toList().get(0).group(1);

            Optional<MatchUser> user = MatchUser.getUser(username);
            return this.getRank(user.map(MatchUser::getElo).orElse(0));
        };
        Messages.registerHook(rankPattern, rankFunction);
    }


    public boolean hasBend(UUID player) throws IOException, TimeoutException, ExecutionException, InterruptedException {
        if (!this.waitingForResponse.containsKey(player))
            this.waitingForResponse.put(player, new CompletableFuture<>());

        CompletableFuture<Boolean> future = this.waitingForResponse.get(player);
        MatchPlugin.getInstance().getRabbitMQ().requestBend(player);
        return future.completeOnTimeout(false, 3, TimeUnit.SECONDS).get();
    }

    public void completeBend(UUID player) {
        if (this.waitingForResponse.containsKey(player))
            this.waitingForResponse.remove(player).complete(true);
    }

    public ProxyServer getServer() {
        return this.server;
    }

    public Logger getLogger() {
        return this.logger;
    }

    public Config getConfig() {
        return this.config;
    }

    public RabbitMQProxy getRabbitMQ() {
        return this.rabbitMQ;
    }

    public HikariConnection getHikariCP() {
        return this.hikariConnection;
    }

    public MatchMaking getMatchMaking() {
        return this.matchMaking;
    }
}
