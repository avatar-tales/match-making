package fr.bakaaless.matchmaking.proxy.config;

import fr.bakaaless.matchmaking.common.configuration.Config;

import java.nio.file.Path;

public enum MatchMessages {

    MATCH_PREFIX("&3&lMatch Making &8&l»"),
    PLAYER("{mm_rank_{0}} {0} &7&l({mm_elo_{0}})"),

    ELO_WIN("{match_prefix} &7Vous avez gagné &b{0} élo&7, vous êtes maintenant {mm_rank_{1}}&7 avec &b{mm_elo_{1}} élo&7."),
    ELO_LOSE("{match_prefix} &7Vous avez perdu &c{0} élo&7, vous êtes maintenant {mm_rank_{1}}&7 avec &b{mm_elo_{1}} élo&7."),
    ELO_STABLE("{match_prefix} &7Quelque chose a bloqué le changement de votre élo..."),

    QUEUE_JOIN("{match_prefix} &7Vous avez rejoint la file d'attente du Ranked. Votre rang actuel est {mm_rank_{0}} &7&l({mm_elo_{0}} élo). Il y a {1} joueurs en attente. &cAttention, afin de vous faire jouer dans les meilleures conditions possibles, les serveurs peuvent mettre jusqu'à 1 minute pour préparer une arène capable de vous accueillir. La patience est une vertu dont tout le monde peut disposer !"),
    QUEUE_LEAVE("{match_prefix} &7Vous avez quitté la file d'attente."),
    INVITATION_RECEIVE("{match_prefix} &b{0} &7veut se batte contre vous en &b{1} &7points gagnants. Faîtes &c/duel accept &7ou &c/duel refuse &7pour accepter ou refuser."),
    INVITATION_SEND("{match_prefix} &7Vous venez de provoquer &b{0} &7en duel en &b{1} &7points gagnants."),
    INVITATION_EXPIRATION("{match_prefix} &b{0} &7n'a pas donné suite à votre provocation."),
    INVITATION_ACCEPT("{match_prefix} &b{0} &7veut bien vous corriger."),
    INVITATION_REFUSE_OTHER("{match_prefix} &b{0} &7ne veut pas être humilié."),
    INVITATION_REFUSE_SELF("{match_prefix} &7Vous avez refusé l'invitation de &b{0}."),

    MATCH_FIND("{match_prefix} &7Vous allez combattre &b{0}&7. Puisse le sort vous être favorable !"),
    ALREADY_PLAYING_SELF("{match_prefix} &cVous êtes déjà en train de jouer !"),
    ALREADY_PLAYING_OTHER("{match_prefix} &cCe joueur joue déjà !"),
    NO_PLAYING("{match_prefix} &cCe joueur ne joue pas !"),
    RANKED_MOD("{match_prefix} &cVous ne pouvez pas aller dans une partie classée !"),
    NO_FOUND("{match_prefix} &cCe joueur n'a pas été trouvé !"),
    NO_ONLINE("{match_prefix} &cCe joueur n'est pas en ligne !"),
    NO_SELF("{match_prefix} &cVous ne pouvez pas vous inviter vous-même."),
    NO_SERVER("{match_prefix} &cIl n'y a aucune arène de disponible. Vous pourrez réessayer dans une à deux minutes."),
    NO_BEND("{match_prefix} &cVous n'avez pas de bending. Vous devez en choisir un pour pouvoir jouer."),
    ;

    private String display;

    MatchMessages(String display) {
        this.display = display;
    }

    public static void init(final String path, final String name) {
        try {
            final Config config = Config.load(Path.of(path), name);
            for (final MatchMessages messages : MatchMessages.values()) {
                final String pathToMessage = messages.name().replace("__", "-").replace("_", ".").toLowerCase();
                messages.display = config.getOrWrite(pathToMessage, messages.display);
            }
            config.save();
        } catch (Exception ignored) {
        }
    }

    public String display() {
        return this.display.replace("{match_prefix}", MATCH_PREFIX.display);
    }
}
