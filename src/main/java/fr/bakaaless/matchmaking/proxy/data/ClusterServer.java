package fr.bakaaless.matchmaking.proxy.data;

import com.velocitypowered.api.proxy.server.RegisteredServer;
import fr.bakaaless.matchmaking.common.games.ArenaSettings;

import java.util.List;

public final class ClusterServer extends Server {

    private List<ArenaSettings> arenas;

    public ClusterServer(final String name, final RegisteredServer server, final List<ArenaSettings> arenas) {
        super(server, name);
        this.arenas = arenas;
        this.lastPing = System.currentTimeMillis();
    }

    public void remove() {
        this.getConnectedPlayers().forEach(matchUser -> {
            matchUser.setPlaying(false);
            matchUser.getPlayer().ifPresent(player ->
                    Server.getLobbies().stream().findAny().ifPresent(lobbyServer ->
                            player.createConnectionRequest(lobbyServer.getServer()).fireAndForget()
                    )
            );
        });
    }

    public List<ArenaSettings> getArenas() {
        return this.arenas;
    }

    public void setArenas(final List<ArenaSettings> arenas) {
        this.arenas = arenas;
    }

}
