package fr.bakaaless.matchmaking.proxy.data;

import com.velocitypowered.api.proxy.server.RegisteredServer;

public final class LobbyServer extends Server {
    public LobbyServer(RegisteredServer server, String name) {
        super(server, name);
    }

    @Override
    public void remove() {
        this.getConnectedPlayers().forEach(matchUser ->
                matchUser.getPlayer().ifPresent(player ->
                        Server.getLobbies().stream().filter(server -> server != this)
                                .findAny()
                                .ifPresent(lobbyServer ->
                                        player.createConnectionRequest(lobbyServer.getServer()).fireAndForget()
                                )
                )
        );
    }
}
