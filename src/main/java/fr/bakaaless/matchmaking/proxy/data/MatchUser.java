package fr.bakaaless.matchmaking.proxy.data;

import com.velocitypowered.api.proxy.Player;
import fr.bakaaless.matchmaking.proxy.MatchPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class MatchUser {

    private final static Map<UUID, MatchUser> USERS = new HashMap<>();

    private final UUID uniqueId;
    private final String name;
    private int elo;
    private boolean playing;
    private boolean ranked;
    private Server connection;

    public MatchUser(final UUID uniqueId, final String name, final int elo) {
        this.uniqueId = uniqueId;
        this.name = name;
        this.elo = elo;
        this.playing = false;
        this.ranked = true;
    }

    public static void register(Player player, int elo) {
        USERS.putIfAbsent(player.getUniqueId(), new MatchUser(player.getUniqueId(), player.getUsername(), elo));
    }

    public static Optional<MatchUser> getUser(UUID uniqueId) {
        return Optional.ofNullable(USERS.get(uniqueId));
    }

    public static Optional<MatchUser> getUser(String name) {
        return USERS.values().stream().filter(matchUser -> matchUser.name.equalsIgnoreCase(name)).findFirst();
    }

    public static void remove(MatchUser user) {
        USERS.remove(user.getUniqueId());
    }

    public UUID getUniqueId() {
        return this.uniqueId;
    }

    public String getName() {
        return this.name;
    }

    public int getElo() {
        return this.elo;
    }

    public void setElo(final int elo) {
        this.elo = Math.max(elo, 0);
    }

    public boolean isPlaying() {
        return this.playing;
    }

    public void setPlaying(boolean playing) {
        this.playing = playing;
    }

    public boolean isRanked() {
        return this.ranked;
    }

    public void setRanked(boolean ranked) {
        this.ranked = ranked;
    }

    public Server getConnection() {
        return this.connection;
    }

    public void setConnection(Server connection) {
        this.connection = connection;
    }

    public Optional<Player> getPlayer() {
        return MatchPlugin.getInstance().getServer().getPlayer(this.uniqueId);
    }

    @Override
    public String toString() {
        return "MatchUser{" +
                "uniqueId=" + uniqueId +
                ", name='" + name + '\'' +
                ", elo=" + elo +
                ", playing=" + playing +
                ", ranked=" + ranked +
                ", connection=" + connection +
                '}';
    }
}
