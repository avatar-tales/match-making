package fr.bakaaless.matchmaking.proxy.data;

import com.velocitypowered.api.proxy.server.RegisteredServer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public abstract sealed class Server permits ClusterServer, LobbyServer {

    private static final List<Server>     SERVERS = new ArrayList<>();
    protected final      RegisteredServer server;
    protected final      List<MatchUser>  connectedPlayers;
    protected            String           name;
    protected            long             lastPing;

    public Server(RegisteredServer server, String name) {
        this.server = server;
        this.name = name;
        this.connectedPlayers = new ArrayList<>();
        this.lastPing = System.currentTimeMillis();
    }

    public static List<Server> getFilteredRawServers(final Function<Server, Boolean> filter) {
        return SERVERS.stream().filter(filter::apply).toList();
    }

    public static List<ClusterServer> getFilteredClusterServers(final Function<ClusterServer, Boolean> filter) {
        return SERVERS.stream()
                .filter(server -> server instanceof ClusterServer)
                .map(server -> (ClusterServer) server)
                .filter(filter::apply)
                .toList();
    }

    public static List<LobbyServer> getLobbies() {
        return SERVERS.stream()
                .filter(server -> server instanceof LobbyServer)
                .map(server -> (LobbyServer) server)
                .toList();
    }

    public static List<Server> getServersByServer(final RegisteredServer registeredServer) {
        return SERVERS.stream().filter(server -> server.getServer().equals(registeredServer)).toList();
    }

    public static void removeServer(String name) {
        SERVERS.removeIf(server -> {
            if (server.getName().equals(name)) {
                server.remove();
                return true;
            }
            return false;
        });
    }

    public static void registerServer(final Server server) {
        SERVERS.add(server);
    }

    public abstract void remove();

    public RegisteredServer getServer() {
        return this.server;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MatchUser> getConnectedPlayers() {
        return this.connectedPlayers;
    }

    public long getLastPing() {
        return this.lastPing;
    }

    public void setLastPing(long lastPing) {
        this.lastPing = lastPing;
    }
}
