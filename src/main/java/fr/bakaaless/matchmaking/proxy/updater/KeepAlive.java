package fr.bakaaless.matchmaking.proxy.updater;

import fr.bakaaless.matchmaking.common.messaging.Message;
import fr.bakaaless.matchmaking.proxy.MatchPlugin;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class KeepAlive implements Runnable {

    @Override
    public void run() {
        try {
            MatchPlugin.getInstance().getRabbitMQ().sendMessage(new Message("*", "proxy", Message.MessageType.PING, ""));
        } catch (IOException | TimeoutException e) {
            throw new RuntimeException(e);
        }
    }

}
