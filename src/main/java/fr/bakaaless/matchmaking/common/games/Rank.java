package fr.bakaaless.matchmaking.common.games;

public class Rank {

    private final String display;
    private final int    elo;

    public Rank(String display, int elo) {
        this.display = display;
        this.elo = elo;
    }

    public String display() {
        return this.display;
    }

    public int elo() {
        return this.elo;
    }

    @Override
    public String toString() {
        return "Rank{" +
                "display='" + display + '\'' +
                ", elo=" + elo +
                '}';
    }
}
