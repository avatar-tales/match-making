package fr.bakaaless.matchmaking.common.games;

import java.util.Arrays;
import java.util.UUID;

public class ArenaSettings {

    private UUID     id;
    private String   name;
    private int      teamsNumber;
    private int      teamSize;
    private int      pointToWin;
    private int      countdown;
    private int      timeBeforeKick;
    private String[] inventory;

    private boolean wildcard = false;

    public static ArenaSettings defaultSettings() {
        ArenaSettings settings = new ArenaSettings();
        settings.id = UUID.randomUUID();
        settings.pointToWin = 3;
        settings.teamSize = 1;
        settings.teamsNumber = 2;
        settings.countdown = 3;
        settings.timeBeforeKick = 10;
        settings.inventory = new String[0];
        return settings;
    }

    public static ArenaSettings wildCard() {
        ArenaSettings settings = defaultSettings();
        settings.wildcard = true;
        return settings;
    }

    public boolean isWildCard() {
        return this.wildcard;
    }

    public boolean matchType(final ArenaSettings o) {
        return this.wildcard || o.wildcard || this.teamsNumber == o.teamsNumber && this.teamSize == o.teamSize && this.pointToWin == o.pointToWin;
    }

    public UUID getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTeamsNumber() {
        return teamsNumber;
    }

    public void setTeamsNumber(int teamsNumber) {
        this.teamsNumber = teamsNumber;
    }

    public int getTeamSize() {
        return teamSize;
    }

    public void setTeamSize(int teamSize) {
        this.teamSize = teamSize;
    }

    public int getPointToWin() {
        return pointToWin;
    }

    public void setPointToWin(int pointToWin) {
        this.pointToWin = pointToWin;
    }

    public int getCountdown() {
        return this.countdown;
    }

    public void setCountdown(int countdown) {
        this.countdown = countdown;
    }

    public int getTimeBeforeKick() {
        return this.timeBeforeKick;
    }

    public void setTimeBeforeKick(int timeBeforeKick) {
        this.timeBeforeKick = timeBeforeKick;
    }

    public String[] getInventory() {
        return this.inventory;
    }

    public void setInventory(String[] inventory) {
        this.inventory = inventory;
    }

    @Override
    public String toString() {
        return "ArenaSettings{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", teamsNumber=" + teamsNumber +
                ", teamSize=" + teamSize +
                ", pointToWin=" + pointToWin +
                ", countdown=" + countdown +
                ", timeBeforeKick=" + timeBeforeKick +
                ", inventory=" + Arrays.toString(inventory) +
                '}';
    }
}
