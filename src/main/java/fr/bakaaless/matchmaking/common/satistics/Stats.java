package fr.bakaaless.matchmaking.common.satistics;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.UUID;

public class Stats {

    public final UUID   uniqueId;
    public final String display;
    public final String name;
    public final int    elo;
    public final int    kills;
    public final int    deaths;
    public final int    wins;
    public final int    games;
    public final int[]  ranks;


    public Stats(final String uniqueId) {
        this.uniqueId = UUID.nameUUIDFromBytes(uniqueId.getBytes(StandardCharsets.UTF_8));
        this.display = "";
        this.name = uniqueId;
        this.elo = 0;
        this.kills = 0;
        this.deaths = 0;
        this.wins = 0;
        this.games = 0;
        this.ranks = new int[]{0, 0, 0, 0, 0};
    }

    public Stats(final UUID uniqueId, final String display, final String name, final int elo, final int kills, final int deaths, final int wins, final int games, final int[] ranks) {
        this.uniqueId = uniqueId;
        this.display = display;
        this.name = name;
        this.elo = elo;
        this.kills = kills;
        this.deaths = deaths;
        this.wins = wins;
        this.games = games;
        this.ranks = ranks;
    }

    @Override
    public String toString() {
        return "Stats{" +
                "uniqueId=" + uniqueId +
                ", display='" + display + '\'' +
                ", name='" + name + '\'' +
                ", elo=" + elo +
                ", kills=" + kills +
                ", deaths=" + deaths +
                ", wins=" + wins +
                ", games=" + games +
                ", ranks=" + Arrays.toString(ranks) +
                '}';
    }
}
