package fr.bakaaless.matchmaking.common.configuration;

import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.yaml.NodeStyle;
import org.spongepowered.configurate.yaml.YamlConfigurationLoader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;

public class Config {

    private final File                    fileConfig;
    private       boolean                 firstConfig;
    private YamlConfigurationLoader loader;
    private       ConfigurationNode       config;

    protected Config(final Path dataDirectory, final String fileName) {
        this.fileConfig = new File(dataDirectory.toFile(), fileName);
        this.firstConfig = false;
        try {
            if (!this.fileConfig.exists()) {
                this.fileConfig.getParentFile().mkdirs();
                this.fileConfig.createNewFile();
                this.firstConfig = true;
            }
            this.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected Config(final Path dataDirectory, final String fileName, final String resourcePath) {
        this(dataDirectory, fileName);
        if (this.isFirstConfig()) {
            try {
                final InputStream link = getClass().getResourceAsStream(resourcePath);
                Files.copy(link, Path.of(dataDirectory.toFile().getAbsolutePath() + "/" + fileName), StandardCopyOption.REPLACE_EXISTING);
                this.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static Config load(final Path dataDirectory, final String fileName) {
        return new Config(dataDirectory, fileName);
    }

    public static Config load(final Path dataDirectory, final String fileName, final String resourcePath) {
        return new Config(dataDirectory, fileName, resourcePath);
    }

    public static <T> void write(final ConfigurationNode root, final String path, final T value) {
        if (path.contains(".")) {
            final String[] split = path.split("\\.");
            ConfigurationNode node = root;
            for (final String s : split) {
                node = node.node(s);
            }
            node.raw(value);
        } else {
            root.node(path).raw(value);
        }
    }

    public static <T> T getOrWrite(final ConfigurationNode root, final String path, final T default_) {
        if (path.contains(".")) {
            final String[] split = path.split("\\.");
            ConfigurationNode node = root;
            for (final String s : split) {
                node = node.node(s);
            }
            if (node.raw() == null)
                node.raw(default_);
            return (T) node.raw();
        } else {
            if (root.node(path).raw() == null)
                root.node(path).raw(default_);
            return (T) root.node(path).raw();
        }
    }

    public <T> void write(final String path, final T value) {
        write(this.config, path, value);
    }

    public void load() throws IOException {
        this.loader = YamlConfigurationLoader.builder().file(this.fileConfig)
                .nodeStyle(NodeStyle.BLOCK)
                .build();
        this.config = this.loader.load();
    }

    public <T> T getOrWrite(final String path, final T default_) {
        return getOrWrite(this.config, path, default_);
    }

    public <T> T get(final String path, final T default_) {
        if (path.contains(".")) {
            final String[] split = path.split("\\.");
            ConfigurationNode node = this.config;
            for (final String s : split) {
                node = node.node(s);
            }
            if (node.raw() == null)
                return default_;
            return (T) node.raw();
        } else {
            if (this.config.node(path).raw() == null)
                return default_;
            return (T) this.config.node(path).raw();
        }
    }

    public List<String> getKeys(final String path) {
        if (path.contains(".")) {
            final String[] split = path.split("\\.");
            ConfigurationNode currentNode = this.config;
            for (final String s : split)
                currentNode = currentNode.node(s);
            return currentNode.childrenMap().keySet().stream().map(Object::toString).toList();
        } else {
            return this.config.node(path).childrenMap().keySet().stream().map(Object::toString).toList();
        }
    }

    public void save() throws IOException {
        this.loader.save(this.config);
    }

    public boolean isFirstConfig() {
        return this.firstConfig;
    }
}


