package fr.bakaaless.matchmaking.common.databases;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.function.Consumer;

public class HikariConnection {

    private final Path             dataDirectory;
    private       HikariConfig     dataConfig;
    private       HikariDataSource dataSource;

    private boolean isSQLite = false;

    public HikariConnection(final Path dataDirectory) {
        this.dataDirectory = dataDirectory;
        this.load();
    }

    public void load() {
        HikariConfiguration config = HikariConfiguration.load(this.dataDirectory, "configs/database.yml");
        this.setupDriver(config);
        this.dataConfig.setMaxLifetime(60000);
        this.dataConfig.setMaximumPoolSize(10);
        this.dataConfig.setAutoCommit(true);
        this.dataSource = new HikariDataSource(this.dataConfig);
        this.createTables();
    }

    private void setupDriver(final HikariConfiguration config) {
        switch (config.getDriver().toLowerCase()) {
            case "sqlite" -> {
                this.dataConfig = new HikariConfig();
                this.dataConfig.setPoolName("MatchMakingSQLitePool");
                this.dataConfig.setDriverClassName("org.sqlite.JDBC");
                this.dataConfig.setJdbcUrl("jdbc:sqlite:" + dataDirectory.toFile().getAbsolutePath() + "/MatchMaking.db");
                this.isSQLite = true;
            }
            case "mariadb" -> {
                this.dataConfig = new HikariConfig();
                this.dataConfig.setDriverClassName("org.mariadb.jdbc.Driver");
                this.dataConfig.setJdbcUrl("jdbc:mariadb://" + config.getHost() + ":" + config.getPort() + "/" + config.getDatabase());
                this.dataConfig.setUsername(config.getUsername());
                this.dataConfig.setPassword(config.getPassword());
                this.dataConfig.setPoolName("MatchMakingMariaDBPool");
            }
            case "postgresql" -> {
                this.dataConfig = new HikariConfig();
                this.dataConfig.setDriverClassName("org.postgresql.ds.PGSimpleDataSource");
                this.dataConfig.setJdbcUrl("jdbc:postgresql://" + config.getHost() + ":" + config.getPort() + "/" + config.getDatabase());
                this.dataConfig.setUsername(config.getUsername());
                this.dataConfig.setPassword(config.getPassword());
                this.dataConfig.setPoolName("MatchMakingPostgreSQLPool");
            }
            default -> throw new IllegalStateException("Unexpected value: " + config.getDriver());
        }
    }

    public Connection getConnection() throws SQLException {
        return this.dataSource.getConnection();
    }

    private void createTables() {
        final InputStream link = getClass().getResourceAsStream("/scripts/tables.sql");
        try (BufferedReader input = new BufferedReader(new InputStreamReader(link))) {
            List<String> requests = new ArrayList<>();
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = input.readLine()) != null) {
                if (line.isBlank()) {
                    requests.add(stringBuilder.toString());
                    stringBuilder = new StringBuilder();
                    continue;
                }
                stringBuilder.append(line).append(" ");
            }
            requests.add(stringBuilder.toString());
            requests.stream().filter(request -> !request.isBlank()).forEach(this::doQuery);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public synchronized void readQuery(final String query, Consumer<ResultSet> toExec, Runnable elseExec) {
        try (Connection connection = this.getConnection()) {
            if (connection == null || connection.isClosed()) {
                elseExec.run();
                return;
            }
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                toExec.accept(stmt.executeQuery());
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        elseExec.run();
    }

    public synchronized void doQuery(final String query) {
        try (Connection connection = this.getConnection()) {
            if (connection == null || connection.isClosed())
                return;

            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.execute();
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        if (!this.dataSource.isClosed())
            this.dataSource.close();
    }

    public boolean isClosed() {
        return this.dataSource.isClosed();
    }

}
