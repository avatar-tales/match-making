package fr.bakaaless.matchmaking.common.databases;

import fr.bakaaless.matchmaking.common.configuration.Config;

import java.nio.file.Path;

public class HikariConfiguration extends Config {

    private HikariConfiguration(final Path dataDirectory, final String fileName) {
        super(dataDirectory, fileName, "/configs/database.yml");

    }

    public static HikariConfiguration load(final Path dataDirectory, final String fileName) {
        return new HikariConfiguration(dataDirectory, fileName);
    }

    public String getDriver() {
        return this.get("driver", "sqlite");
    }

    public String getHost() {
        return this.get("host", "localhost");
    }

    public String getPort() {
        return this.get("port", "3306");
    }

    public String getDatabase() {
        return this.get("database", "dbName");
    }

    public String getUsername() {
        return this.get("user", "root");
    }

    public String getPassword() {
        return this.get("password", "root");
    }

}
