package fr.bakaaless.matchmaking.common.messaging.messages.game;

import fr.bakaaless.matchmaking.common.messaging.messages.Packet;

import java.util.UUID;

public class AddSpectators implements Packet {

    private final UUID player;
    private final UUID[] spectators;

    public AddSpectators(UUID player, UUID[] spectators) {
        this.player = player;
        this.spectators = spectators;
    }

    public UUID getPlayer() {
        return this.player;
    }

    public UUID[] getSpectators() {
        return this.spectators;
    }
}
