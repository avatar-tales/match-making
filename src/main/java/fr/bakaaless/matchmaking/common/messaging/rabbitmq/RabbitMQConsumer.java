package fr.bakaaless.matchmaking.common.messaging.rabbitmq;

import fr.bakaaless.matchmaking.common.messaging.Message;

import java.util.function.Consumer;

public abstract class RabbitMQConsumer implements Consumer<Message> {

    protected final RabbitMQ instance;

    public RabbitMQConsumer(final RabbitMQ rabbitMQ) {
        this.instance = rabbitMQ;
    }


}
