package fr.bakaaless.matchmaking.common.messaging.messages.other;

import fr.bakaaless.matchmaking.common.messaging.messages.Packet;

import java.util.UUID;

public class HasBending implements Packet {

    private final UUID player;

    public HasBending(UUID player) {
        this.player = player;
    }

    public UUID getPlayer() {
        return this.player;
    }
}
