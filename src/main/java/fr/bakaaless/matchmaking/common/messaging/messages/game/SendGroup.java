package fr.bakaaless.matchmaking.common.messaging.messages.game;

import fr.bakaaless.matchmaking.common.games.ArenaSettings;
import fr.bakaaless.matchmaking.common.games.Rank;
import fr.bakaaless.matchmaking.common.messaging.messages.Packet;

import java.util.Map;
import java.util.UUID;

public class SendGroup implements Packet {

    private final UUID[] players;
    private final Map<UUID, Rank> ranks;
    private final ArenaSettings arenaSettings;
    private final boolean ranked;
    private final int rounds;

    public SendGroup(final UUID[] players, Map<UUID, Rank> ranks, final ArenaSettings arenaSettings, boolean ranked, int rounds) {
        this.players = players;
        this.ranks = ranks;
        this.arenaSettings = arenaSettings;
        this.ranked = ranked;
        this.rounds = rounds;
    }

    public UUID[] getPlayers() {
        return this.players;
    }

    public Map<UUID, Rank> getRanks() {
        return this.ranks;
    }

    public ArenaSettings getArenaSettings() {
        return this.arenaSettings;
    }

    public boolean isRanked() {
        return this.ranked;
    }

    public int getRounds() {
        return this.rounds;
    }
}
