package fr.bakaaless.matchmaking.common.messaging.messages.lobby;

import fr.bakaaless.matchmaking.common.messaging.messages.Packet;
import fr.bakaaless.matchmaking.common.satistics.Stats;

import java.util.List;

public class TopPlayerResponse implements Packet {

    private final String      mode;
    private final List<Stats> result;


    public TopPlayerResponse(String mode, List<Stats> result) {
        this.mode = mode;
        this.result = result;
    }

    public String getMode() {
        return this.mode;
    }

    public List<Stats> getResult() {
        return this.result;
    }
}
