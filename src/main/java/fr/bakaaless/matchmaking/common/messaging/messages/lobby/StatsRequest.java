package fr.bakaaless.matchmaking.common.messaging.messages.lobby;

import fr.bakaaless.matchmaking.common.messaging.messages.Packet;

public class StatsRequest implements Packet {

    private final String user;

    public StatsRequest(String user) {
        this.user = user;
    }

    public String getUser() {
        return this.user;
    }
}
