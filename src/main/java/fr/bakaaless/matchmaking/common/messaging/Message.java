package fr.bakaaless.matchmaking.common.messaging;

public class Message {

    private final String      target;
    private final String      source;
    private final MessageType type;
    private final String      content;

    public Message(final String target, final String source, final MessageType type, final String content) {
        this.target = target;
        this.source = source;
        this.type = type;
        this.content = content;
    }

    public String target() {
        return this.target;
    }

    public String source() {
        return this.source;
    }

    public MessageType type() {
        return this.type;
    }

    public String content() {
        return this.content;
    }

    @Override
    public String toString() {
        return "Message{" +
                "target='" + this.target + '\'' +
                ", source='" + this.source + '\'' +
                ", type=" + this.type +
                ", content='" + this.content + '\'' +
                '}';
    }

    public enum MessageType {

        ERROR,
        HAS_BENDING,
        PING,
        PONG,
        REQUEST_BENDING,
        SEND_GROUP,
        START_GAME,
        EVENT,
        ADD_SPECTATOR,
        REMOVE_SPECTATOR,
        STOPPING_SERVER,
        END,

        ADD_QUEUE,
        REMOVE_QUEUE,
        STATS_REQUEST,
        STATS_RESPONSE,
        TOP_PLAYERS_REQUEST,
        TOP_PLAYERS_RESPONSE

    }
}
