package fr.bakaaless.matchmaking.common.messaging.rabbitmq;

public record RabbitMQProperties(String name, String host, int port, String username, String password,
                                 String queueName) {
}
