package fr.bakaaless.matchmaking.common.messaging.messages.game;

import fr.bakaaless.matchmaking.common.messaging.messages.Packet;

import java.util.UUID;

public class Event implements Packet {

    private final UUID        gameId;
    private final UUID        player;
    private final EventAction action;
    private final long        timestamp;

    public Event(UUID gameId, UUID player, EventAction action) {
        this.gameId = gameId;
        this.player = player;
        this.action = action;
        this.timestamp = System.currentTimeMillis();
    }

    public UUID getGameId() {
        return this.gameId;
    }

    public UUID getPlayer() {
        return this.player;
    }

    public EventAction getAction() {
        return this.action;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public enum EventAction {
        KILL,
        DEATH
    }
}
