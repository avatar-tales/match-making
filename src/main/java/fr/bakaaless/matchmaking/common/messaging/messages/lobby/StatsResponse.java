package fr.bakaaless.matchmaking.common.messaging.messages.lobby;

import fr.bakaaless.matchmaking.common.messaging.messages.Packet;
import fr.bakaaless.matchmaking.common.satistics.Stats;

public class StatsResponse implements Packet {

    private final String id;
    private final Stats  stats;

    public StatsResponse(String id, Stats stats) {
        this.id = id;
        this.stats = stats;
    }

    public String getId() {
        return this.id;
    }

    public Stats getStats() {
        return this.stats;
    }
}
