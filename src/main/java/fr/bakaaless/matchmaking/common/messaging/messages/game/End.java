package fr.bakaaless.matchmaking.common.messaging.messages.game;

import fr.bakaaless.matchmaking.common.messaging.messages.Packet;

import java.util.UUID;

public class End implements Packet {

    private final UUID gameId;
    private final UUID[] victory;
    private final UUID[] defeat;
    private final UUID[] spectators;
    private final EndReason reason;
    private final boolean ranked;

    public End(UUID gameId, final UUID[] victory, final UUID[] defeat, UUID[] spectators, EndReason reason, boolean ranked) {
        this.gameId = gameId;
        this.victory = victory;
        this.defeat = defeat;
        this.spectators = spectators;
        this.reason = reason;
        this.ranked = ranked;
    }

    public UUID getGameId() {
        return this.gameId;
    }

    public UUID[] getVictory() {
        return this.victory;
    }

    public UUID[] getDefeat() {
        return this.defeat;
    }

    public UUID[] getSpectators() {
        return this.spectators;
    }

    public EndReason getReason() {
        return this.reason;
    }

    public boolean isRanked() {
        return this.ranked;
    }

    public enum EndReason {

        LEGIT,
        INTERRUPT

    }
}
