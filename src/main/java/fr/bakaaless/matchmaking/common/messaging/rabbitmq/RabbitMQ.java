package fr.bakaaless.matchmaking.common.messaging.rabbitmq;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import fr.bakaaless.matchmaking.common.messaging.Message;
import fr.bakaaless.matchmaking.common.messaging.messages.Packet;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class RabbitMQ {

    private final Gson               gson;
    private       RabbitMQProperties properties;
    private       RabbitMQConsumer   consumer;

    public RabbitMQ(final Path directory) {
        this.loadProperties(RabbitConf.load(directory, "configs/rabbitmq.yml").getRabbitMQProperties());
        this.gson = new GsonBuilder()
                .disableHtmlEscaping()
                .serializeNulls()
                .create();
    }

    public void loadProperties(final RabbitMQProperties properties) {
        this.properties = properties;
        try {
            final Connection connection = this.getConnection();
            final Channel channel = connection.createChannel();
            final Map<String, Object> args = new HashMap<>();
            args.put("x-message-ttl", 5000);
            channel.queueDeclare(this.properties.name(), false, false, false, args);
            channel.queueBind(this.properties.name(), this.properties.queueName(), this.properties.queueName());
            channel.close();
            connection.close();
        } catch (IOException | TimeoutException e) {
            throw new RuntimeException(e);
        }
    }

    public Connection getConnection() throws IOException, TimeoutException {
        final ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(this.properties.host());
        factory.setPort(this.properties.port());
        factory.setUsername(this.properties.username());
        factory.setPassword(this.properties.password());
        return factory.newConnection();
    }

    public void sendMessage(final Message message) throws IOException, TimeoutException {
        final Connection connection = this.getConnection();
        final Channel channel = connection.createChannel();
        channel.basicPublish(this.properties.queueName(), this.properties.queueName(), null, this.gson.toJson(message).getBytes(StandardCharsets.UTF_8));
        channel.close();
        connection.close();
    }

    public void sendMessage(String target, String source, Message.MessageType type, Packet packet) throws IOException, TimeoutException {
        final Connection connection = this.getConnection();
        final Channel channel = connection.createChannel();
        channel.basicPublish(this.properties.queueName(), this.properties.queueName(), null, this.gson.toJson(new Message(target, source, type, this.gson.toJson(packet))).getBytes(StandardCharsets.UTF_8));
        channel.close();
        connection.close();
    }

    public Thread retrieveMessages(final RabbitMQConsumer callback) {
        this.consumer = callback;
        return new Thread(() -> {
            try {
                final Channel channel = this.getConnection().createChannel();
                channel.basicConsume(this.properties.name(), true, (consumerTag, message) -> {
                            final Message data = this.gson.fromJson(new String(message.getBody(), StandardCharsets.UTF_8), Message.class);
                            if (data.target().equals(this.properties.name()) || data.target().equalsIgnoreCase("*"))
                                callback.accept(data);
                        },
                        System.err::println
                );
            } catch (IOException | TimeoutException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public RabbitMQProperties getProperties() {
        return this.properties;
    }

    public Gson getGson() {
        return this.gson;
    }

    public RabbitMQConsumer getConsumer() {
        return this.consumer;
    }
}
