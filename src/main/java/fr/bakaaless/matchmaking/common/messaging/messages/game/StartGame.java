package fr.bakaaless.matchmaking.common.messaging.messages.game;

import fr.bakaaless.matchmaking.common.messaging.messages.Packet;

import java.util.List;
import java.util.UUID;

public class StartGame implements Packet {

    private final String       arenaName;
    private final UUID         gameId;
    private final List<UUID[]> teams;
    private final long         timestamp;
    private final boolean      ranked;

    public StartGame(String arenaName, UUID gameId, List<UUID[]> teams, boolean ranked) {
        this.arenaName = arenaName;
        this.gameId = gameId;
        this.teams = teams;
        this.timestamp = System.currentTimeMillis();
        this.ranked = ranked;
    }

    public String getArenaName() {
        return this.arenaName;
    }

    public UUID getGameId() {
        return this.gameId;
    }

    public List<UUID[]> getTeams() {
        return this.teams;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public boolean isRanked() {
        return this.ranked;
    }
}
