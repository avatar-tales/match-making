package fr.bakaaless.matchmaking.common.messaging.messages.game;

import fr.bakaaless.matchmaking.common.messaging.messages.Packet;

import java.util.UUID;

public class RemoveSpectators implements Packet {

    private final UUID[] spectators;

    public RemoveSpectators(UUID[] spectators) {
        this.spectators = spectators;
    }

    public UUID[] getSpectators() {
        return this.spectators;
    }
}
