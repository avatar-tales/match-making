package fr.bakaaless.matchmaking.common.messaging.messages.lobby;

import fr.bakaaless.matchmaking.common.messaging.messages.Packet;

public class TopPlayersRequest implements Packet {

    private final String mode;
    private final int    limit;

    public TopPlayersRequest(String mode, int limit) {
        this.mode = mode;
        this.limit = limit;
    }

    public String getMode() {
        return this.mode;
    }

    public int getLimit() {
        return this.limit;
    }
}
