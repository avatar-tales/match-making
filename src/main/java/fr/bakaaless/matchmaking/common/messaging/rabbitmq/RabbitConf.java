package fr.bakaaless.matchmaking.common.messaging.rabbitmq;

import fr.bakaaless.matchmaking.common.configuration.Config;

import java.nio.file.Path;
import java.util.UUID;

public class RabbitConf extends Config {

    private RabbitConf(final Path dataDirectory, final String fileName) {
        super(dataDirectory, fileName, "/configs/rabbitmq.yml");
    }

    public static RabbitConf load(final Path dataDirectory, final String fileName) {
        return new RabbitConf(dataDirectory, fileName);
    }

    public RabbitMQProperties getRabbitMQProperties() {
        return new RabbitMQProperties(
                getOrWrite("serverName", UUID.randomUUID().toString()),
                getOrWrite("host", "localhost"),
                getOrWrite("port", 5672),
                getOrWrite("user", "guest"),
                getOrWrite("password", "guest"),
                getOrWrite("queueName", "matchmaking")
        );
    }
}
