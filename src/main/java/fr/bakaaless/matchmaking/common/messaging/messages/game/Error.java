package fr.bakaaless.matchmaking.common.messaging.messages.game;

import fr.bakaaless.matchmaking.common.messaging.messages.Packet;

public class Error implements Packet {

    private final ErrorType type;

    public Error(ErrorType type) {
        this.type = type;
    }

    public enum ErrorType {

    }
}
