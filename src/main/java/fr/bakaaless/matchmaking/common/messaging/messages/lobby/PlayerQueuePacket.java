package fr.bakaaless.matchmaking.common.messaging.messages.lobby;

import fr.bakaaless.matchmaking.common.messaging.messages.Packet;

import java.util.UUID;

public class PlayerQueuePacket implements Packet {

    private final UUID user;

    public PlayerQueuePacket(UUID user) {
        this.user = user;
    }

    public UUID getUser() {
        return this.user;
    }
}
