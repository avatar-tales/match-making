package fr.bakaaless.matchmaking.common.messaging.messages;

import fr.bakaaless.matchmaking.common.games.ArenaSettings;

import java.util.List;

public class PongPacket implements Packet {

    private final String              name;
    private final int                 port;
    private final List<ArenaSettings> arenas;
    private final boolean             lobby;

    public PongPacket(final String name, final int port, final List<ArenaSettings> arenas, final boolean lobby) {
        this.name = name;
        this.port = port;
        this.arenas = arenas;
        this.lobby = lobby;
    }

    public String getName() {
        return this.name;
    }

    public int getPort() {
        return this.port;
    }

    public List<ArenaSettings> getArenas() {
        return arenas;
    }

    public boolean isLobby() {
        return this.lobby;
    }
}
