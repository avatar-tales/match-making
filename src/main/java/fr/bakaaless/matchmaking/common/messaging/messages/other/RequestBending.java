package fr.bakaaless.matchmaking.common.messaging.messages.other;

import fr.bakaaless.matchmaking.common.messaging.messages.Packet;

import java.util.UUID;

public class RequestBending implements Packet {

    private final UUID player;

    public RequestBending(final UUID bending) {
        this.player = bending;
    }

    public UUID getPlayer() {
        return this.player;
    }
}
